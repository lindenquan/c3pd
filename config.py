from os.path import abspath, dirname


class DevelopmentConfig:
  DEBUG = True          # It should be false in production mode
  ENABLE_CACHE = False  # It should be true in production mode
  PORT = 7000
  TEMPLATE = 'templates'
  SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@localhost/pedigree?charset=utf8'
  SQLALCHEMY_TRACK_MODIFICATIONS = True
  WTF_CSRF_ENABLED = True
  SECRET_KEY = 'you-will-never-guess'
  DATE_FORMAT = '%Y/%m/%d %H:%M:%S'
  HOST = '0.0.0.0'
  PROGRAM_HOME = abspath(dirname(__file__)) + '/app'
  LOG_FILE = PROGRAM_HOME + '/website.log'
  DEFAULT_CROP = 'Flax'
  # Attribute in the filter list would not be displayed on the basic
  # information section on cultivar page
  FILTER_LIST = ['explode', 'card', 'image', 'card_id', 'crop_type_id', 'id_cultivar', 'metadata', 'query', 'query_class', 'cultivar_name', 'used_names',
                 'parent1', 'parent2', 'year_registered', 'year_released', 'year_started', 'improvement_status', 'breeding_method', 'origin', 'year_group']


class ProductionConfig(DevelopmentConfig):
  DEBUG = False
  ENABLE_CACHE = True
