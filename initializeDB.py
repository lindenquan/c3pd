#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import db, app, models
from app.db.DB_Proxy import dbProxy
import re
from os import listdir, makedirs
from os.path import isdir
from sets import Set
import time
from sys import stdout
import threading
import codecs

class bcolors:
  HEADER = '\033[95m'
  OKBLUE = '\033[94m'
  OKGREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  ENDC = '\033[0m'
  BOLD = '\033[1m'
  UNDERLINE = '\033[4m'

start_time = time.time()
working = True
APP_HOME = app.config['PROGRAM_HOME']
IMAGE_FOLDER = APP_HOME + '/static/images/cultivar/'
MISSING = 'missing'

db.drop_all()
db.create_all()

# key is name
# value is cultivar object
cultivarCache = {}
missingCultivars = Set()


def elapsedTime():
  while(working):
    elapse = time.time() - start_time
    minute = int(elapse / 60)
    second = int(elapse - minute * 60)
    elapse = str(second) + ' sec'
    if minute != 0:
      elapse = str(minute) + ' min, ' + elapse
    stdout.write('\rElapsed time: ' + bcolors.FAIL + elapse + bcolors.ENDC)
    stdout.flush()
    time.sleep(5)
  print ''


def writeProgress(taskName, fileName):
  stdout.write("\r parsing " + bcolors.OKBLUE + taskName +
               bcolors.ENDC + " " + bcolors.WARNING + fileName + bcolors.ENDC)
  stdout.flush()
  print ''


def getCleanName(name):
  # ABC(1990) => ABC
  name = re.sub(r"\(\d+\)", '', name)
  # "ABC" => ABC
  if name[0] == '"' or name[0] == "'":
    name = name[1:]
  if name[-1] == '"' or name[-1] == "'":
    name = name[:-1]
  return name.strip()


def addOption(searchItem, option):
  options = searchItem.options
  if len(options) == 0:
    searchItem.options = option
  else:
    options = options.split(models.Search.delimiter)
    if option in options:
      return
    options.append(option)
    searchItem.options = models.Search.delimiter.join(options)


def addNewCultivar(cropID, cultivarName):
  cultivar = db.createModel(models.Cultivar, crop_type_id=cropID)
  cultivar.cultivar_name = cultivarName

#
# parents = {}
# key is cultivar id
# value is [cultivar, parent1_name, parent2_name]
#


def addParentID(cropID, parents):
  # nameId = {cultivar name : id }
  nameId = {}
  for id, list in parents.iteritems():
    nameId[list[0].cultivar_name] = id
  for id, list in parents.iteritems():
    if list[1] != '':
      parentID = nameId.get(list[1])
      if parentID == None:
        missingCultivars.add(list[1])
        #addNewCultivar(cropID, list[1])
      else:
        list[0].parent1 = parentID
    if list[2] != '':
      parentID = nameId.get(list[2])
      if parentID == None:
        missingCultivars.add(list[2])
        #addNewCultivar(cropID, list[2])
      else:
        list[0].parent2 = parentID


def tranferFromFileToDB(crop, file):
  writeProgress('main table file', file)
  cropName = crop.strip().lower()
  cropImageFolder = IMAGE_FOLDER + cropName
  if not isdir(cropImageFolder):
    makedirs(cropImageFolder)
  cropType = db.getOrCreateModel(models.CropType, name=cropName)
  db.session.flush()
  with codecs.open(file, "r", "utf-8") as f:
    count = 0
    columns = []
    lenColum = 0
    searchColumns = {}
    parents = {}

    for line in f:
      count += 1
      values = line.strip().split('\t')

      if count == 1:
        lenColum = len(values)
        for i in range(lenColum):
          v = values[i]
          v = v.lower().replace(' ', '_')
          if v[-1] == '*':
            v = v[:-1]
            searchColumns[i] = db.createModel(
                models.Search,   crop_type_id=cropType.id_crop_type, attribute=v, options='')
          columns.append(v)
        continue

      if len(values[0].strip()) == 0:
        continue
      cultivar = db.createModel(
          models.Cultivar, crop_type_id=cropType.id_crop_type)
      db.session.flush()
      parents[cultivar.id_cultivar] = [cultivar, '', '']

      for i in range(len(values)):
        if i >= lenColum:
          break
        value = values[i].replace('"', '').strip()
        if len(value) == 0:
          continue

        colName = columns[i]
        if(colName == 'parent1'):
          parents[cultivar.id_cultivar][1] = value
        elif(colName == 'parent2'):
          parents[cultivar.id_cultivar][2] = value
        elif(colName == 'cultivar_name'):
          cultivar.cultivar_name = getCleanName(value)
          cultivar.lowercase_name = cultivar.cultivar_name.lower()
        else:
          setattr(cultivar, columns[i], value)
        if i in searchColumns:
          addOption(searchColumns[i], value)
  addParentID(cropType.id_crop_type, parents)
  db.session.flush()
  printMissingCultivars(file)


def findKey(line):
  parts = line.split('**')
  if len(parts) >= 3:
    return parts[1].strip()
  else:
    return None


def isInvalidLine(line):
  line = line.strip().replace(' ', '')
  if re.search('^-+$', line):
    return True


def getCultivarByName(cropID, name):
  name = getCleanName(name)
  cultivar = cultivarCache.get(name)
  if cultivar == None:
    cultivar = db.session.query(models.Cultivar).filter_by(
        crop_type_id=cropID, cultivar_name=name).first()
    if cultivar == None:
      cultivarCache[name] = MISSING
    else:
      cultivarCache[name] = cultivar
  elif cultivar == MISSING:
    return None
  return cultivar


def cardToDB(cropID, file):
  keyValue = {}
  currentKey = ''
  value = ''
  isBreak = False
  with codecs.open(file, "r", "utf-8") as f:
    count = 0
    for line in f:
      count += 1
      if count == 1:
        # skip the first line
        continue
      line = line.strip()
      if isInvalidLine(line):
        isBreak = True
        continue
      key = findKey(line)
      if key is not None:
        currentKey = key
        value = line[line.index('**', 1) + 2:].strip().replace('^', '')
        if value[:2] == '**':
          value = value[2:]
        if value[-2:] == '**':
          value = value[:-2]
        keyValue[key] = value
        isBreak = False
      elif not isBreak and currentKey != '' and len(line.strip()) != 0:
        keyValue[currentKey] += ('\n' + line.strip()).replace('^', '')
  card = db.createModel(models.Card)
  db.session.flush()
  cultivarName = keyValue.get('Cultivar name', None)

  for k, v in keyValue.iteritems():
    if len(v.strip()) != 0:
      k = k.strip().replace(' ', '_').lower()
      if k == 'reference':
        k = 'references'
      setattr(card, k, v)
  cultivar = getCultivarByName(cropID, cultivarName)
  if cultivar == None:
    missingCultivars.add(cultivarName)
  else:
    card.cultivar_id = cultivar.id_cultivar
    cultivar.card_id = card.id_card


def parseCardFolder(cropName, folder):
  writeProgress('card folder', folder)
  cropID = db.session.query(models.CropType).filter_by(
      name=cropName.strip().lower()).first().id_crop_type
  for file in listdir(folder):
    if re.match('\w+.*.txt', file):
      cardToDB(cropID, folder + file)
  printMissingCultivars(folder)


def getCultivarIDByName(cropID, name):
  cultivar = getCultivarByName(cropID, name)
  if cultivar != None:
    return cultivar.id_cultivar
  return None


def parseCoefficient(cropName, file):
  writeProgress('COP file', file)
  cropID = db.session.query(models.CropType).filter_by(
      name=cropName.strip().lower()).first().id_crop_type
  # each item is "smaller cultivarID,bigger cultivarID"
  # cache =['15,234','1,5234']
  cache = Set()
  with codecs.open(file, "r", "utf-8") as f:
    countRow = 0
    row1 = None
    countCol = 0
    for line in f:
      countRow += 1
      if countRow == 1:
        row1 = [item.strip() for item in line.split('\t')]
        countCol = len(row1) - 1
        continue
      else:
        row = [item.strip() for item in line.split('\t')]
        cultivar1 = getCleanName(row[0])
        for col in range(countCol):
          col += 1
          cultivar2 = getCleanName(row1[col])
          efficient = float(row[col])
          cultivar1_ID = getCultivarIDByName(cropID, cultivar1)
          cultivar2_ID = getCultivarIDByName(cropID, cultivar2)
          if cultivar1_ID == None:
            missingCultivars.add(cultivar1)
          if cultivar2_ID == None:
            missingCultivars.add(cultivar2)
          if cultivar1_ID != None and cultivar2_ID != None:
            strId = None
            if cultivar1_ID < cultivar2_ID:
              strId = str(cultivar1_ID) + ',' + str(cultivar2_ID)
            else:
              strId = str(cultivar2_ID) + ',' + str(cultivar1_ID)
            if strId not in cache:
              cache.add(strId)
              if cultivar1_ID < cultivar2_ID:
                db.createModel(models.Coefficient, cultivar1_id=cultivar1_ID,
                               cultivar2_id=cultivar2_ID, coefficient=efficient)
              else:
                db.createModel(models.Coefficient, cultivar1_id=cultivar2_ID,
                               cultivar2_id=cultivar1_ID, coefficient=efficient)
  db.session.flush()
  printMissingCultivars(file)


def printMissingCultivars(fileName):
  global missingCultivars
  if len(missingCultivars) != 0:
    print 'missing cultivars in ', fileName
    print missingCultivars
    missingCultivars = Set()

#########################  Main ###############################
threading.Thread(target=elapsedTime).start()

tranferFromFileToDB('Flax', 'file/flax_pedigree_data7.0_for_pedigree_website.txt')
tranferFromFileToDB('Barley', 'file/barley_pedigree_data_a_1.3_for_pedigree_website.txt')
tranferFromFileToDB('Wheat', 'file/wheat_pedigree_data_3.1_for_pedigree_website.txt')

parseCardFolder('flax', 'file/Flax_cultivar_cards/')
parseCardFolder('barley', 'file/barley_cultivar_cards/')
parseCardFolder('wheat', 'file/wheat_cultivar_cards/')

parseCoefficient('flax', 'file/flax_pedigree_COPs.txt')
parseCoefficient('barley', 'file/barley_pedigree_COPs.txt')
parseCoefficient('wheat', 'file/wheat_pedigree_COPs.txt')

db.session.commit()

working = False
