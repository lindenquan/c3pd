Canadian Crop Cultivar Pedigree Database (C3PD)
===============================================

# Table contents
[TOC]

## Overview
This is a web application that delivers information of crop pedigrees.

## Software design and implementation
#### Basic idea and algorithms
1. Read text files and store the data into MySQL.
2. According to the given cultivar, server-side will retrieve data from MySQL, and then assemble corresponding HTML and send them back to client-side with JavaScript.
3. An cultivar object is like this:
```python
class Cultivar {
 String name;
 Integer year;
 ...
 Cultivar parent1;
 CUltivar parent2;
} 
```

And a cultivar pedigree tree is like this:

               cultivar
                /     \
             parentA  parentB
             /    \     /  \
          parentC parentD  None

This tree is traversed by an algorithm which is similar with BFS (Breadth First Search).

The traversal algorithm is like this:
```python
queue = [cultivar]

while (queue.length != 0) {
  cultivar = queue.popFirst();
  # do something with cultivar depending on situation.
  if (cultivar.parent1 != None){
    queue.putLast(cultivar.parent1)
  }
  if (cultivar.parent2 != None){
    queue.putLast(cultivar.parent2)
  }
}
```
#### Technologies, languages
1.Server-side:
 Technologies: Apache2.4, MySQL 5.7
 Languages: Python 2.7, flask framework, jinja2 

2.Client-side:
 HTML5, jQuery, Stylus(CSS)

## How to install 
#### Step 1 Install MySQL 5.7 above
##### Set MySQL default character as UTF-8 
```
sudo vim /etc/mysql/my.cnf
```
Insert the following two lines:
```
[mysqld]
character-set-server = utf8
```
#### Step 2 Install python 2.7
```sh
$ sudo apt-get install python-setuptools python-dev python-MySQLdb libssl-dev
```
#### Step 3 Install python libraries
```sh
sudo pip install -r file/requirements.txt 
```

## How to update code

All what you need to do is run **sync.sh** script with root account.

#### Sync.sh will do the following things:
1. Pull latest code from bitbucket
2. Change the website to production mode
3. Change the ownership of all files and folders to www-date
4. Restart apache2

#### Sync.sh comes with the following constraints:
1. Must run the script with root account.
2. Must run the script under c3pd folder.

## How to convert XLSX files to UTF8 text files
Make sure all files are in UTF-8 format
#### Step 1 Open xlsx file with microsoft Excel software

#### Step 2 Save as Text(Tab delimited)(*.txt)
You will see "Confirm Save As" dialogue, Click Yes, and Click Yes again at the following dialogue.
Don't select "Unicode Text(*.txt)" type. Somehow, it doesn't work very well.

#### Step 3 Put the txt files in the Linux, and use any software you like to convert txt file to UTF-8 format.
I use sublime text to save the text files as UTF8 format.

## Usage

#### How to input the data to database
Run "initializeDB.py" script. It will find main pedigree text files, cop files and card folder in the './file' folder, and then store them into database.

#### Images

All cultivar images are in the "c3pd/app/static/images/cultivar" folder.  

For example, if there is flax cultiver called ABC, its image should be "c3pd/app/static/images/cultivar/flax/ABC.jpg".  

The image file extension must be 'jpg' with lower case.  

And its svg file should be "c3pd/app/static/images/cultivar/flax/pedigree/ABC.svg"   

The svg file extension must be 'svg' with lower case.