  Ager
  ------

  **Cultivar name**             Ager
  ----------------------------- -------------------------------------------------------------
  **Used names**                CI 13861
  **Origin**                    France
  **Year released**             1963
  **Year started**              
  **Affiliation of breeders**   Institute de la Recherche Agronomique
  **Breeding method**           Pedigree
  **Use type**                  Six-rowed, Spring
  **Pedigree**                  
  **Female parent**             
  Name:                         Bordia/ Kenia
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         Weihenstephan 259-711
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.ars-grin.gov/cgi-bin/npgs/acc/display.pl?1253213


