Johnston

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Johnston
  ----------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                CI 15850

  **Origin**                    Manitoba Canada

  **Year released**             1980

  **Year started**              1971

  **Affiliation of breeders**   Agriculture Canada Research Station, Brandon, Manitoba

  **Breeding method**           Bulk

  **Use type**                  six-rowed feed spring barley

  **Pedigree**                  

  **Female parent**             

  Name:                         Klondike (BT 323)

  Origin:                       Canada 1976

  Use type:                     Six-row spring barley

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         Nord/3/Vantage/Jet//Vantmore/4/Bonanza

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      Johnston originated from the cross Klondike/5/ Nord/3/Vantage/Jet//Vantmore/4/Bonanza. The final cross was made in the autumn of 1971. It was increased from a single Fo plant selected from a space-seeded bulk of 500 handselected seeds of the cross, sown at Brandon in 1973. Special attention was given in early stages of development to selection for clean well-filled seed, and in the field, for vigorous clean-leaved plants.

  **Pedigree tree**             

  **Performance**               Six-rowed, smooth-awned, sub-standard malting barley. Spike mid-lax and semi-erect. Kernels hulled, mid-size to small with white aleurone, short rachila hairs and smooth to moderately wrinkled hulls. Adapted to the Canadian prairies, espeically central Alberta. Outyields Klondike between 5-14% in tests. Resistant to Puccinia graminis, Ustilago nuda and Helminthosporium spp. Moderate resistance to Rhynochosporium secalis. Susceptible to U. hordei, U. nigra, Pyrenophora teres and Septoria passerinii. Matures 5 days later than Bonanza. W

  **Reference**                 Research Station, Agriculture Canada, Brandon, Manitoba R7A 527. Received 5 May 1980,.accepted 9 June 1980.
                                
                                https://npgsweb.ars-grin.gov/gringlobal/accessiondetail.aspx?id=1073633
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


