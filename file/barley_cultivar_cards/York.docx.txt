  York
  ------

  **Cultivar name**             York
  ----------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                C.I. 10075
  **Origin**                    Canada
  **Year released**             1958
  **Year started**              
  **Affiliation of breeders**   The Ontario Agricultural College. Guelph, Ontario. Canada
  **Breeding method**           pedigree
  **Use type**                  Six-row, spring
  **Pedigree**                  
  **Female parent**             
  Name:                         Stephan / Galore
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         O.A.C. 21 / Peatland
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      York (C.I. 10075) (C.A.N. 239) was developed at the Ontario Agricultural College. Guelph, Ontario. Canada, from the cross Stephan / Galore/2/ O.A.C. 21 / Peatland. It was licensed for sale in Canada in 1958 as a feed barley.
  **Pedigree tree**             
  **Performance**               High yield potential, strong straw, good threshability, and resistance to stem rust and powdery mildew.
  **Reference**                 Classification of BARLEY VARIETIES grown in the United States and Canada in 1958


