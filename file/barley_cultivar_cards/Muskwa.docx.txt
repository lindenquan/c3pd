  Muskwa
  --------

  **Cultivar name**             Muskwa
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                BT584/ H97090012/FB427
  **Origin**                    Canada
  **Year released**             2013
  **Year started**              1997
  **Affiliation of breeders**   James H. Helm, Alberta Agriculture and Rural Development, Lacombe, Alberta
  **Breeding method**           Bulk
  **Use type**                  six row, spring feed-type barley
  **Pedigree**                  
  **Female parent**             
  Name:                         Tukwa
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         SD513
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      'Muskwa' (experimental designation BT584, H97090012, and FB427) was developed at the Field Crop Development Center, Lacombe, Alberta using a modified bulk pedigree method. It arose from the cross 'Tukwa' / SD513 conducted in 1997. The F2 generation was grown at Lacombe in 1998, and the F3 was grown in El Centro, California during the winter of 1998-99. The F5 to F7 generations were grown in bulk populations at Lacombe from 1999 to 2002. From the F7 bulk, 200 heads were selected to be grown as F8 headrows in 2003. From these headrows, the line H97090012 was selected to be grown in yield trials from 2004-10. While in yield trials, purification nurseries were grown at each level of yield testing for removal of variants and to compile detailed descriptions. In 2007, 200 heads were grown out from a bulk increase plot as individual rows. Selected heads provided the source for a Pre-Breeder Headrow nursery in 2008, and breeder rows and plots in 2009. These plots were bulked to form the first breeder seed of the variety, made up of 200 F15 breeding lines. In 2009, this line was tested as BT584 in the Western Six Row Barley Cooperative Test and as FB427 in the Western Forage Barley Cooperative Test. 'Muskwa' was supported for registration by the Prairie Recommending Committee for Oats and Barley in 2011. Selection criteria included grain yield, test weight, 1000 kernel weight, lodging resistance, disease resistance and maturity.
  **Pedigree tree**             
  **Performance**               good resistance to lodging and shattering, good tolerance to straw breakage and drought, poor malting quality
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/bar/app00008458e.shtml


