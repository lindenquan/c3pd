  Charlottetown 80
  ------------------

  **Cultivar name**             Charlottetown 80
  ----------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                C.I. 2732
  **Origin**                    Canada
  **Year released**             
  **Year started**              
  **Affiliation of breeders**   Experimental Farm, Charlottetown, Prince Edward Island, Canada.
  **Breeding method**           Field selection
  **Use type**                  Two-rowed rough-awned spring barley
  **Pedigree**                  
  **Female parent**             
  Name:                         Old Island Two-Rowed
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      History.—According to Harlan, Martini, and Pope (43), Charlottetown 80 (CI. 2732) (C.A.N. 1100) is a selection from Old Island Two-Rowed, made by J. A. Clark, Superintendent of the Experimental Farm, Charlottetown, Prince Edward Island, Canada. The Old Island Two-Rowed has been grown on Prince Edward Island for many years. It consists principally of Chevalier types but also contains a much taller barley with a dense spike. Mr. Clark selected one of the Chevalier types that had a tendency to drop its awns at maturity. Old Island Two-Rowed undoubtedly was brought there from England.
  **Pedigree tree**             
  **Performance**               Charlottetown 80 has a moderately stiff straw.
  **Reference**                 Classification of BARLEY VARIETIES grown in the United States and Canada in 1958


