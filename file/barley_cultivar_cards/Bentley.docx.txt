  Bentley
  ---------

  **Cultivar name**             Bentley
  ----------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                TR05669/ FB414/ H93103
  **Origin**                    Canada
  **Year released**             2010
  **Year started**              1994
  **Affiliation of breeders**   James H. Helm, Alberta Agriculture and Rural Development, Lacombe, Alberta
  **Breeding method**           Bulk
  **Use type**                  two row, spring malting barley
  **Pedigree**                  
  **Female parent**             
  Name:                         I92125
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         TR229
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      'Bentley' (experimental designations FB414, H93103, and TR05669) was developed at the Field Crop Development Center, Lacombe, Alberta, using a modified bulk pedigree system. It arose from the cross I92125/TR229 in 1993. The F2-F6 generations were grown in bulk from 1994 to 1998. The line H93103004 was selected from the F6-F7 to be grown in yield trials from 2000 to 2007. Selection criteria included grain yield, forage yield, test weight, 1000 kwt, lodging resistance, disease resistance, maturity, and malting quality. It was further tested as TR05669 in the Western Two Row Barley Cooperative, and as FB414 in the Western Forage Barley Cooperative in 2005.
  **Pedigree tree**             
  **Performance**               'Bentley' has strong intensity of anthocyanin colouration on the auricules of the flag leaves whereas it is medium in 'AC Metcalfe'.
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/bar/app00009299e.shtml


