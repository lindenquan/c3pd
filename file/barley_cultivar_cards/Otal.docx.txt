Otal

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Otal
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                PGR 1977/CI 15853

  **Origin**                    Canada

  **Year released**             1981

  **Year started**              1967

  **Affiliation of breeders**   United States Department of Agriculture and the state Agricultural and Forestry Experiment Station, Palmer, Alaska.

  **Breeding method**           Pedigree

  **Use type**                  six-rowed spring feed barley

  **Pedigree**                  

  **Female parent**             

  Name:                         Otra

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         Weibull 1514-64

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      Otal was derived from a cross made in Alaska in 1967 between the Finnish cultivar Otra and Weibull 1514-64, a selection of the Swedish Weibull sholm Plant Breeding Institute. Weillbulllsholm Plant Breeding Institute. Weibull 1514-64 is selection from the cross: Maja/3/Hanna/Svanhals//Opal/4/Tammi(Weibull 5672/5/Morgenrot. Single plant selections were made in the F2 through F5 generations. An F6, designated Alaska 71-II-67-18-57, was harvested and tested as such in Alaska from 1975 to1980 and as NRG B77-10 and BT655 in Canada. Breeder seed was developed from a bulk of more than 400 F~10~ single plant selections. Otal has also been listed in United States and Canadian national collections as CI 15853 and PGR 1977, respectively.

  **Pedigree tree**             

  **Performance**               combination of earliness and high yield

  **Reference**                 R. I. WOLFE', R. L. TAYLOR2, and
                                
                                D. G. FARIS3
                                
                                lAgriculture Canada Research Station,
                                
                                Box 29, Beaverlodge, Alberta, Canada
                                
                                T1H 0C0; 2Agriculture and Forestry Experi'
                                
                                ment Station, Palmer Research Center,
                                
                                533 E. Fireweed, Palmer, Alaska 99645,
                                
                                U.S.A.; and 3International Crops Research
                                
                                Institute for the Semi-Arid Tropics (ICRISAT),
                                
                                Pantancheru PO, Andhra Pradesh 502 324
                                
                                India. Received I3 Feb. 1987, accepted
                                
                                11 Mav 1987.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


