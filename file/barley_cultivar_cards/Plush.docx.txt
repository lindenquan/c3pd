  Plush
  -------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Plush
  ----------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                C.I. 6093

  **Origin**                    Canada

  **Year released**             1938

  **Year started**              

  **Affiliation of breeders**   Dominion Experimental Farm, Brandon, Manitoba

  **Breeding method**           pedigree

  **Use type**                  Six-rowed smooth-awned spring barley

  **Pedigree**                  

  **Female parent**             

  Name:                         Lion

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         Bearer

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      Plush (C.I. 6093) (C.A.N. 1106) is of hybrid origin and came from the cross Lion X Bearer. It originated at the Dominion Experimental Farm, Brandon, Manitoba. The Bearer parent came from the cross Blue Long Head X Gordon made in 1903 at the Central Experimental Farm at Ottawa. Gordon also is a hybrid, its parents being Baxter's Six-Row and Duckbili. The final cross from which Plush originated
                                
                                was made by S. J. Sigfusson at the Brandon station. Plush was released to growers in 1938.

  **Pedigree tree**             

  **Performance**               Plush has a moderate to stiff straw. The hulls are less firmly attached to the caryopsis than they are in other commonly grown varieties like O.A.C. 21 and Manchuria

  **Reference**                 Classification of BARLEY VARIETIES grown in the United States and Canada in 1958
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


