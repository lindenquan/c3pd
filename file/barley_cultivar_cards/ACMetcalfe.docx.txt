AC Metcalfe

  -------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             AC Metcalfe
  ----------------------------- -------------------------------------------------------------------------------------------------------------------------
  **Used names**                TR232

  **Origin**                    Canada

  **Year released**             1997

  **Year started**              1986

  **Affiliation of breeders**   the Agriculture and Agri-Food Canada (AAFC) Research Centre, Brandon, MB, and AAFC Cereal Research Centre, Winnipeg, MB

  **Breeding method**           pedigree

  **Use type**                  two-row spring malting barley

  **Pedigree**                  

  **Female parent**             

  Name:                         AC Oxbow

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         Manley

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      AC Metcalfe was developed from the cross AC
                                
                                Oxbow/Manley made in 1986 at the AAFC Cereal Research
                                
                                Centre, Winnipeg, MB. Early generations were handled by
                                
                                a conventional pedigree method. The F1 and F2 generations
                                
                                were grown in growth cabinets. The F2 plants were inoculated
                                
                                with loose smut spores \[*Ustilago nuda* (Jens.) Rostr.\].
                                
                                F3 plants with loose smut resistance were selected in the
                                
                                greenhouse and inoculated again with *U. nuda* spores. F4
                                
                                plants with loose smut resistance were selected in the greenhouse,
                                
                                and 500 single F5 progeny rows were grown in the
                                
                                field in 1987. Progeny rows were selected on the basis of
                                
                                height, maturity, lodging resistance, general appearance,
                                
                                resistance to net blotch (*Pyrenophora teres* Drechs.) and
                                
                                preliminary malting quality analyses (i.e., alpha amylase
                                
                                activity, diastatic power, fine grind extract and amino nitrogen
                                
                                concentration), and grown as F6 bulk rows in the winter
                                
                                nursery at Brawley, California, USA. Selected lines, one of
                                
                                which was WM8612-1, were transferred to the AAFC
                                
                                Research Centre, Brandon, MB, where they were grown in
                                
                                replicated yield tests in 1988 and 1989. Selection was made
                                
                                using the same criteria plus yield, heading date, test weight,
                                
                                kernel weight, advanced malting quality analyses (i.e.,
                                
                                above quality traits plus coarse grind extract, difference
                                
                                between fine and coarse extracts, soluble protein concentration
                                
                                and viscosity), and resistance to stem rust (*Puccinia*
                                
                                *graminis* Pers.), common root rot \[*Cochliobolus sativus* (Ito
                                
                                & Kurib.) Drechs. ex Dast.\] and net blotch. WM8612-1 was
                                
                                grown in the 1990 Eastern Prairie Barley Test at seven locations
                                
                                in Manitoba and Saskatchewan, and evaluated for the
                                
                                same traits as in 1989 plus kernel plumpness and resistance
                                
                                to loose smut, covered smut \[*U. hordei* (Pers.) Lagerh.\],
                                
                                false loose smut (*U. nigra* Tapke), scald \[*Rhynchosporium*
                                
                                *secalis* (Oud.) J.J. Davis\], and speckled leaf blotch (*Septoria*
                                
                                *passerinii* Sacc.). It was advanced in 1991 to the Western
                                
                                Cooperative Two-row Barley Registration Test, where it
                                
                                was evaluated for 3 years as TR232.

  **Pedigree tree**             

  **Performance**               high yield, good agronomic traits, moderate disease resistance and excellent malting quality

  **Reference**                 **Mather, D. E., Tinker, N. A., LaBerge, D. E., Edney, M., Jones,**
                                
                                **B. L., Rossnagel, B. G., Legge, W. G., Briggs, K. G., Irvine, R.**
                                
                                **B., Falk, D. E. and Kasha, K. J. 1997.** Regions of the genome that
                                
                                affect grain and malt quality in a North American two-row barley
                                
                                cross. Crop Sci. **37**: 544–554.
                                
                                **Tekauz, A., McCallum, B. and Gilbert, J. 2000.** Review:
                                
                                Fusarium head blight of barley in western Canada. Can. J. Plant
                                
                                Pathol. **22**: 9–16.
                                
                                Can.
  -------------------------------------------------------------------------------------------------------------------------------------------------------


