  Stander
  ---------

  **Cultivar name**             Stander
  ----------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                PI564743
  **Origin**                    Canada
  **Year released**             1993
  **Year started**              
  **Affiliation of breeders**   Minnesota Agricultural Experiment Station
  **Breeding method**           Pedigree
  **Use type**                  6-row malting barley
  **Pedigree**                  
  **Female parent**             
  Name:                         Bumper/Robust
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         Excel
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**       Stander barley was released by the Minnesota Agricultural Experiment Station in 1993.  Stander, from the cross Bumper/Robust/Excel, is a 6-row malting barley.  It has smooth awns, short achilla hairs and colorless aluerone.  Lodging appears superior to Robust but similar to Robust in % plump kernels, spot blotch and net blotch.  Application has been made for Plant Variety Protection (PVP).  Stander can be sold only as a class of certified seed and is subject to the Minnesota Variety Development Fee.
  **Pedigree tree**             
  **Performance**               smooth awns, short achilla hairs and colorless aluerone.  Lodging appears superior to Robust but similar to Robust in % plump kernels, spot blotch and net blotch.
  **Reference**                 <http://www.ag.ndsu.nodak.edu/aginfo/seedstock/Brochures/stander.htm>


