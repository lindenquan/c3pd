Island

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Island
  ----------------------------- ----------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                AB214-2

  **Origin**                    Canada

  **Year released**             2002

  **Year started**              1989

  **Affiliation of breeders**   Eastern Canada Barley Breeding Group, Agriculture and Agri-Food Canada

  **Breeding method**           Pedigree

  **Use type**                  two-row, spring feed barley

  **Pedigree**                  

  **Female parent**             

  Name:                         TBR635-25

  Origin:                       

  Use type:                     

  Improvement status:           advanced line

  Performance:                  

  **Male parent**               

  Name:                         Symko

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      Island was derived from a TBR635-25/Symko cross made at
                                
                                Charlottetown during the winter of 1989–1990. TBR635-25
                                
                                was an advanced line from a CGB83-46/Rodeo cross developed
                                
                                by W. G. Thompson & Sons Limited. CGB83-46 had
                                
                                the following pedigree: UPBS60/UPBS76//UPBS60/UPBS66. The F1 to F6 generations were grown in the field
                                
                                at Charlottetown from 1990 to 1995. Mass selection for
                                
                                head type, lodging resistance, and disease resistance was
                                
                                conducted in the F3, F4, F5, and F6 generations. One F4-
                                
                                derived F7 line, AB214-2, was tested in a preliminary test at
                                
                                Charlottetown in 1996 and was promoted to the Maritime
                                
                                Two-Row Barley Screening Test in 1997. AB214-2 was
                                
                                tested in the Maritime Two-Row Barley Registration- Recommendation Test and in the Quebec Two-Row Barley
                                
                                Registration-Recommendation Test in 1998–2000. In
                                
                                Ontario, AB214-2 was tested in Preliminary Tests in 1998
                                
                                and in Advanced Tests in 1999–2000. AB214-2 was evaluated
                                
                                for resistance to fusarium head blight and deoxynivalenol
                                
                                accumulation in the field under artificial inoculation
                                
                                conditions in five environments \[Ottawa, Charlottetown,
                                
                                and St-Hyacinthe in 2000, Hangzhou (China) in the winter
                                
                                of 2000–2001, and St-Hyacinthe in 2001\]. AB214-2
                                
                                received support for registration from the Atlantic Field
                                
                                Crops Committee, Quebec Cereal Crops Committee, and
                                
                                Ontario Cereal Crops Committee, and it was subsequently
                                
                                registered as Island.

  **Pedigree tree**             

  **Performance**               high yield, high test weight, high seed weight, excellent resistance to powdery mildew and moderate resistance to fusarium head blight

  **Reference**                 Choo, T. M., Martin, R. A., ter Beek, S. M., Ho, K. M., Caldwell, C. D., Walker, D., Rodd, V., Dion, Y. and Rioux, S. 2003.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------


