  EMPRESS
  ---------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             EMPRESS
  ----------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                P.I.473575

  **Origin**                    Canada

  **Year released**             1982

  **Year started**              1970

  **Affiliation of breeders**   Alberta Agriculture Crop Research, Lacombe, Alberta, Canada

  **Breeding method**           Bulk

  **Use type**                  six-rowed, rough-awned, lax nodding headed, early maturing, spring feed barley cultivar

  **Pedigree**                  

  **Female parent**             

  Name:                         66-289-1507/Luther

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         Conquest

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      'EMPRESS' BARLEY *(Hordeum vulgare* L.) (Reg. no. 187), P.I.
                                
                                473575, was developed by Alberta Agriculture Crop Research,
                                
                                Lacombe, Alberta, Canada. It was selected from a top cross of 66-289 1507/'Luther'//'Conquest'. The crosses were made at Oregon State University, Corvallis, Oreg. in 1970 and 1971. The line 66-289-1507 is a short
                                
                                natural mutant of 'Wocus' barley found at Klamath, Ore.
                                
                                Luther is a winter barley derived from an induced mutation
                                
                                of 'Alpine' winter barley. The cross was carried as a modified
                                
                                bulk in the F2 and F3 generations. Single head to row
                                
                                selection was employed in the Fs, F4, and F5 generations.
                                
                                A single head selection from an F5 row fixed the type. F8
                                
                                heads were grown out for elimination of deviants or offtype
                                
                                material and 227 head rows were bulked in 1981 to
                                
                                form breeder seed.

  **Pedigree tree**             

  **Performance**               Empress has shown good adult plant resistance to both scald and net blotch,

  **Reference**                 Head of research and plant breeder, plant breeder, breeding technologist, breeding technologist, and breeding technologist, quality laboratory technologist, and breeding technologist, respectively. Alberta Agriculture Crop Res., Cereal Breeding Program, Bag Service \#47 Lacombe, Alberta, Canada. T0C 1S0. Registration by Crop Sci. Soc. of Am. Accepted 6 June 1983
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


