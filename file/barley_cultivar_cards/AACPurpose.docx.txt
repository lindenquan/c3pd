AAC Purpose

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             AAC Purpose
  ----------------------------- --------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                CH9929n-3

  **Origin**                    Canada

  **Year released**             2014

  **Year started**              2002

  **Affiliation of breeders**   Eastern Cereal and Oilseed Research Centre, Agriculture and Agri-Food Canada

  **Breeding method**           Mass selection

  **Use type**                  two-row, spring feed barley

  **Pedigree**                  

  **Female parent**             

  Name:                         Danuta

  Origin:                       2000 Canada

  Use type:                     Two-row, spring

  Improvement status:           

  Performance:                  Highest susceptibility with an average of 12.6%.

  **Male parent**               

  Name:                         Chief

  Origin:                       2003 Canada

  Use type:                     Two-row, spring feed barley

  Improvement status:           

  Performance:                  high yield, high seed weight, and good resistance to powdery mildew and *Pythium* root rot

  **Pedigree**                  

  **Pedigree description**      AAC Purpose was derived from a cross between Danuta
                                
                                and Chief (Choo et al. 2006) made at the Crops and
                                
                                Livestock Research Centre, Charlottetown, Prince Edward
                                
                                Island, in the fall of 2002. The F1 to F6 generations were
                                
                                grown in the field at Harrington, Prince Edward Island,
                                
                                from 2003 to 2008. Mass selection for head type, lodging
                                
                                resistance, and disease resistance was conducted in the
                                
                                F4, F5, and F6 generations. One F4-derived F7 line,
                                
                                CH2309-2, was tested in a preliminary test at Harrington
                                
                                in 2009 and was promoted to the Maritime Two-Row
                                
                                Barley Screening Test in 2010. CH2309-2 was evaluated
                                
                                in the Maritime Two-Row Barley Registration and
                                
                                Recommendation Test and in the Quebec Two-Row
                                
                                Barley Registration and Recommendation Test from
                                
                                2011 to 2013. CH2309-2 was subsequently named AAC
                                
                                Purpose.

  **Pedigree tree**             

  **Performance**               It has high grain yield, very high seed weight, and resistance to powdery mildew and leaf rust. AAC Purpose performs well in the Maritimes
                                
                                and Quebec.

  **Reference**                 Abdel-Aal, E.-S. M. and Choo, T. M. 2014. Differences in
                                
                                compositional properties of a hulless barley cultivar grown in
                                
                                23 environments in eastern Canada. Can. J. Plant Sci. 94:
                                
                                807\_815.
                                
                                Choo, T. M., Ho, K. M., Martin, R. A., ter Beek, S. M.,
                                
                                Dion, Y. and Rioux, S. 2006. Chief barley. Can. J. Plant Sci. 86:
                                
                                485\_487.
                                
                                Choo, T. M., Martin, R. A., ter Beek, S. M., Ho, K. M.,
                                
                                Caldwell, C. D., Walker, D., Rodd, V., Dion, Y. and Rioux, S.
                                
                                2003\. Island barley. Can. J. Plant Sci. 83: 793\_795.
                                
                                Choo, T. M., Martin, R. A., Xue, A., MacDonald, D., Scott, P.,
                                
                                Rowsell, J., Dion, Y. and Rioux, S. 2014. AAC Mirabel barley.
                                
                                Can. J. Plant Sci. 94: 465\_468.
                                
                                Choo, T. M., ter Beek, S., Martin, R. A., Ho, K. M., Caldwell,
                                
                                C. D., Walker, D. and Rodd, V. 2000. AC Queens barley. Can.
                                
                                J. Plant Sci. 80: 335\_336.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------


