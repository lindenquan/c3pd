  Oderbrucker
  -------------

  **Cultivar name**             Olderbrucker
  ----------------------------- -------------------------------------------------------------
  **Used names**                CI 1174
  **Origin**                    US
  **Year released**             1915
  **Year started**              
  **Affiliation of breeders**   University of Wisconsin
  **Breeding method**           Plant selection
  **Use type**                  Six-rowed, Spring
  **Pedigree**                  
  **Female parent**             
  Name:                         selection from 'Oderbrucker'
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Moderately stiff to stiff straw
  **Reference**                 http://www.ars-grin.gov/cgi-bin/npgs/acc/display.pl?1011918


