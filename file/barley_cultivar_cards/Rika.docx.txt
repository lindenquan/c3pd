  Rika
  ------

  **Cultivar name**             Rika
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                
  **Origin**                    Sweden
  **Year released**             1951
  **Year started**              
  **Affiliation of breeders**   W.Weibull Seed Company
  **Breeding method**           Pedigree
  **Use type**                  Two-rowed, Spring barley
  **Pedigree**                  
  **Female parent**             
  Name:                         Kenia
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         Isaria
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Trails carried out on two sites and over two years showed that the stocks were still morphologically identical and produced the same yield. Apparently the long lasting separate maintenance of the three stocks had no effect on the genetical composition for morphology and yield ability.
  **Reference**                 http://www.ars-grin.gov/cgi-bin/npgs/acc/display.pl?1253203


