  Godiva
  --------

  **Cultivar name**             Godiva
  ----------------------------- ---------------------------------------------------------------------------------------------
  **Used names**                CI 10641
  **Origin**                    US
  **Year released**             1965
  **Year started**              
  **Affiliation of breeders**   Utah Agric. Exp. Station and the USDA-ARS-CR
  **Breeding method**           Pedigree
  **Use type**                  Six-rowed, Hull-ness barley
  **Pedigree**                  
  **Female parent**             
  Name:                         [Bonneville](http://www.ars-grin.gov/cgi-bin/npgs/html/grin-acid.pl?Bonneville+19333+ped)
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         [Nepal 1](http://www.ars-grin.gov/cgi-bin/npgs/html/grin-acid.pl?Nepal1+19333+ped)
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Some resistance to races of the smuts and powdery mildew present in the inter-mountain area
  **Reference**                 http://www.ars-grin.gov/cgi-bin/npgs/acc/display.pl?1064798


