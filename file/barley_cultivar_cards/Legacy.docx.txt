  Legacy
  --------

  **Cultivar name**             Legacy
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                BT950
  **Origin**                    US/Canada
  **Year released**             2001
  **Year started**              1989
  **Affiliation of breeders**   Michael Bjarko, Busch Agricultural Resources LLC, Fort Collins, United States of America
  **Breeding method**           Pedigree
  **Use type**                  six-row spring, malting barley
  **Pedigree**                  
  **Female parent**             
  Name:                         Bumper/Karl//Bumper/Manker/3/Bumper/Karl
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         Excel
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      The pedigree of 'Legacy' is Bumper/Karl//Bumper/Manker/3/Bumper/Karl/4/Excel. The original cross was made in 1989. 'Legacy', which was tested under the experimental designation 6B93-2978 originated in Moorhead, Minnesota as an F4 headrow. It was first tested in replicated yield trials in 1993 at Moorhead and Crookston, Minnesota. The selection was entered in the Mississippi Valley Uniform Regional Barley Nursery in 1996. In 1997 it was entered in the Western Regional Cooperative Trial. In the winter of 1996/1997, headrows were grown in the greenhouse at Fort Collins, Colorado. During the 1997/1998 winter season breeder seed was produced near Yuma, Arizona. Headrows are maintained for breeder seed production.
  **Pedigree tree**             
  **Performance**               fair resistance to lodging and shattering and poor to fair tolerance to straw breaking, neck breaking and drought. The variety has good malting quality
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/bar/app00002298e.shtml


