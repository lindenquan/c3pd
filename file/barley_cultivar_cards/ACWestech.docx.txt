AC Westech

  ------------------------------------------------------------------------------------------------------------
  **Cultivar name**             AC Westech
  ----------------------------- ------------------------------------------------------------------------------
  **Used names**                DB169

  **Origin**                    Canada

  **Year released**             1998

  **Year started**              1987

  **Affiliation of breeders**   Eastern Cereal and Oilseed Research Centre, Agriculture and Agri-Food Canada

  **Breeding method**           doubled-haploid

  **Use type**                  doubled-haploid derived, six-row, spring feed barley

  **Pedigree**                  

  **Female parent**             

  Name:                         Etienne

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         KY63-1294

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      AC Westech was derived from the cross Etienne/KY63-
                                
                                1294\. KY63-1294 was introduced from the International
                                
                                Centre for Agricultural Research in the Dry Areas, and
                                
                                reported as a line with very broad geographical adaptation
                                
                                and intermediate tolerance to barley yellow dwarf virus
                                
                                (Comeau and St-Pierre 1984). The cross was made at
                                
                                Charlottetown during the winter of 1987–1988. The F1 seed
                                
                                was sent to Westech Agriculture Limited, Prince Edward
                                
                                Island, for production of doubled-haploid lines by the *H.*
                                
                                *bulbosum* method (Kasha and Kao 1970). Seed of the doubled-
                                
                                haploid lines was multiplied in Brawley, California,
                                
                                during the winter of 1989–1990. One doubled-haploid line,
                                
                                DB169, was tested in a preliminary test at Charlottetown in
                                
                                1990 and promoted to the Maritime Barley Screening Test
                                
                                in 1991. DB169 was tested for 3 yr (1992–1994) in the
                                
                                Maritime Six-Row Barley Registration Test and subsequently
                                
                                registered as AC Westech.

  **Pedigree tree**             

  **Performance**               high yield, good test weight, and lodging resistance

  **Reference**                 **Comeau, A. and St-Pierre, C. A. 1984.** Trials on the resistance of
                                
                                cereals to barley yellow dwarf virus, report no. 6. Ste-Foy
                                
                                Research Centre, Ste-Foy, QC.
                                
                                **Kasha, K. J. and Kao, K. N. 1970.** High frequency haploid production
                                
                                in barley (*Hordeum vulgare* L.). Nature **225**: 874–876.
  ------------------------------------------------------------------------------------------------------------


