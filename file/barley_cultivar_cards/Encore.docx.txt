Encore

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Encore
  ----------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                AB183-5

  **Origin**                    Canada

  **Year released**             2004

  **Year started**              1986

  **Affiliation of breeders**   Eastern Canada Barley Breeding Group, Agriculture and Agri-Food Canada

  **Breeding method**           Pedigree

  **Use type**                  six-row, spring feed barley

  **Pedigree**                  

  **Female parent**             

  Name:                         Cadette

  Origin:                       Eastern Canada

  Use type:                     Commercial Cultivar

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         QB198.39

  Origin:                       1986 Eastern Canada: QB58.14/Beacon//BT904

  Use type:                     

  Improvement status:           advanced breeding line

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      Encore was derived from a Cadette/QB198.39 cross made at
                                
                                Charlottetown, Prince Edward Island, during the winter of
                                
                                1986–1987. Cadette was a commercial cultivar in Eastern
                                
                                Canada. The other parent, QB198.39, was a first-year entry in
                                
                                the 1986 Eastern Canada Six-Row Barley Co-operative Test,
                                
                                and it was derived from a QB58.14/Beacon//BT904 cross by
                                
                                the Ste-Foy Research Centre, Agriculture and Agri-Food
                                
                                Canada. The F1 to F6 generations were grown in the field at
                                
                                Charlottetown from 1987 to 1992 and the F7 at Harrington,
                                
                                Prince Edward Island, in 1994. Mass selection for head type,
                                
                                lodging resistance, and disease resistance was conducted in the
                                
                                F4, F5, F6 and F7 generations. Twelve F4-derived lines were
                                
                                selected in the F7 generation and were entered to a preliminary
                                
                                test at Harrington in 1995. One of which (AB183-5) was
                                
                                repeated in a preliminary test at Harrington in 1996, and then
                                
                                promoted to the Maritime Six-Row Barley Screening Test
                                
                                from 1997 to 1998. AB183-5 was tested in the Ontario Six-
                                
                                Row Barley Advanced Test from 1999 to 2000 and in the
                                
                                Ontario Six-Row Barley Screening Test from 2001 to 2002. It
                                
                                was tested in the Quebec Six-Row Barley Registration-
                                
                                Recommendation Test from 2000 to 2004. AB183-5 was subsequently
                                
                                registered as Encore.

  **Pedigree tree**             

  **Performance**               high yield and good resistance to lodging

  **Reference**                 Choo, T. M., Ho, K. M., ter Beek, S. M., Martin, R. A., Dion, Y. and Rioux, S. 2006. **Encore barley**. Can. J. Plant Sci. **86**: 489–491.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------


