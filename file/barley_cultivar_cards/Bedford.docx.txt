Bedford

  **Cultivar name**             Bedford
  ----------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                Br. A31 - 1
  **Origin**                    Canada
  **Year released**             1979
  **Year started**              1969
  **Affiliation of breeders**   the Agriculture Canada Research Station, Brandon, Manitoba, which is part of the Eastern Prairie Barley Group
  **Breeding method**           Bulk
  **Use type**                  six-rowed barley
  **Pedigree**                  
  **Female parent**             
  Name:                         Keystone/4/Vantage/jet//Vantmore/3/2\*Husky
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         Cree
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      Bedford originated from the cross Keystone/ 4 lY antage I J etl lY antmorc I 3 I 2\*Husky | 5 | Cree, completed in 1969. It was increased from a single Fa plant selected along with 47 others from a space-seeded bulk of 400 hand selected seeds of the cross, sown at Brandon in 1971. Special attention was given to selection of vigorous clean-leaved plants resistant to nodal collapse at maturity. Bedford was tested in a replicated trial at Brandon in 1973 and it ranked fourth of 64 entries, and was advanced to the 30-cultivar Eastern Prairie Barley Group trials at eight sites in 1974, where it ranked third.
  **Pedigree tree**             
  **Performance**               High yield, straw and good test weight
  **Reference**                 Research Station, Agriculture Canada, Brandon, Manitoba R7A 527. Received 8 Aug. 1979, accepted 30 Apr. 1980.


