  Gitane
  --------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Gitane
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                PI 428487

  **Origin**                    Netherlands

  **Year released**             1976

  **Year started**              

  **Affiliation of breeders**   Wiersum, [National Small Grains Collection](https://npgsweb.ars-grin.gov/gringlobal/site.aspx?id=19)

  **Breeding method**           pedigree

  **Use type**                  2-rowed spring barley

  **Pedigree**                  

  **Female parent**             

  Name:                         Zephyr (CI 13667)

  Origin:                       Netherlands 1965

  Use type:                     Six-row spring barley

  Improvement status:           

  Performance:                  Trails carried out on two sites and over two years showed that the stocks were still morphologically identical and produced the same yield. Apparently the long lasting separate maintenance of the three stocks had no effect on the genetical composition for morphology and yield ability.

  **Male parent**               

  Name:                         CI1237/Cambrinus

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      

  **Pedigree tree**             

  **Performance**               moderately susceptible to Stripe Rust

  **Reference**                 <http://genbank.vurv.cz/barley/pedigree/krizeni2.asp?id=978>
                                
                                <https://npgsweb.ars-grin.gov/gringlobal/accessiondetail.aspx?id=1323532>
                                
                                ttp://www.ars-grin.gov/cgi-bin/npgs/acc/display.pl?1253203
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


