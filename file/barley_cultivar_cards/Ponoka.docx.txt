  Ponoka
  --------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Ponaka
  ----------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                TR01656/ H93003006Z

  **Origin**                    Canada

  **Year released**             2005

  **Year started**              1993

  **Affiliation of breeders**   Patricia Juskiw, Alberta Agriculture and Rural Development, Lacombe, Alberta

  **Breeding method**           single seed descent

  **Use type**                  two row, spring barley

  **Pedigree**                  

  **Female parent**             

  Name:                         H92001F1

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         TR229

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      'Ponoka', which was known by the experimental designation TR01656 or the Alberta Agriculture line \# H93003006Z, originated from the cross H92001F1 / 'TR229' made at the Field Crop Development Center, Lacombe, Alberta using the single seed descent breeding method.
                                
                                H92001F1 was created from the cross 'Harrington' / 'Camelot'. 'Harrington' (TR441) is a two row malting barley registered in 1981 by the University of Saskatchewan, Saskatoon, Saskatchewan. It was a cross of 'Klages' / S7211. 'Camelot' was introduced to Lacombe's breeding program in 1991 from ICARDA/CIMMYT 13th International Barley Yield Test. This line originated in Germany. 'TR229' is a two row malting barley tested in the Western Cooperative Two-row barley registration test from 1990 to 1992 and granted interim registration in 1993. It is a cross of 'AC Oxbow' ('TR226')/ 'Manley'(TR490) made by Dick Metcalfe and Bill Legge of Agriculture & Agri-Food Canada, Brandon, Manitoba.
                                
                                Heads from the F2 bulk of the population H93003 were selected for a single seed descent program in 1994. The F3 and F4 plants were grown in growth chambers in Saskatoon, Saskatchewan in the winter of 1994-95. The F5 head rows of this population were grown at Lacombe, Alberta in the summer of 1995. From these head rows the line H93003006Z was selected to enter into yield testing from 1996-2002. Purification of this line took place during this period being completed in 2001 with the bulking of 195 breeder lines at the F11 level to form the breeder seed of this variety. Selection criteria used for this variety included yield, test weight, 1000 kwt, lodging resistance, disease resistance, and maturity. Malting quality was evaluated as well, although the line did not meet the criteria for a malting barley. In 2001, this line was entered into the Western Two Row Barley Cooperative test as TR01656.

  **Pedigree tree**             

  **Performance**               fair lodging resistance and tolerance to drought, good shattering resistance, and tolerance to straw and neck breaking. 'Ponoka' has poor malting quality

  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/bar/app00004286e.shtml
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


