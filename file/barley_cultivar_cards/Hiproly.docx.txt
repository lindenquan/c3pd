  Hiproly
  ---------

  ----------------------------------------------------------------------------------------------------------
  **Cultivar name**             Hiproly
  ----------------------------- ----------------------------------------------------------------------------
  **Used names**                CI 3947

  **Origin**                    

  **Year released**             

  **Year started**              

  **Affiliation of breeders**   

  **Breeding method**           Pedigree

  **Use type**                  

  **Pedigree**                  

  **Female parent**             

  Name:                         LV-ETH

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      

  **Pedigree tree**             

  **Performance**               

  **Reference**                 <http://www.ars-grin.gov/cgi-bin/npgs/html/grin-acid.pl?Hiproly+19333+ped>
                                
                                genbank.vurv.cz
  ----------------------------------------------------------------------------------------------------------


