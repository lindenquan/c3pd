  Opal B
  --------

  **Cultivar name**             Opal B
  ----------------------------- ------------------------------------------------------------------------
  **Used names**                CI 6617
  **Origin**                    Sweden
  **Year released**             1934
  **Year started**              
  **Affiliation of breeders**   Swedish Seed Association-Svalov
  **Breeding method**           Single selection
  **Use type**                  Two-row spring barley
  **Pedigree**                  
  **Female parent**             Opal
  Name:                         
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.ars-grin.gov/cgi-bin/npgs/html/grin-acid.pl?OpalB+19333+ped


