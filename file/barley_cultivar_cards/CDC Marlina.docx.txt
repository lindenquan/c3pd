  CDC Marlina
  -------------

  **Cultivar name**             CDC Marlina
  ----------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                HB11316/SH080213
  **Origin**                    Canada
  **Year released**             2014
  **Year started**              2005
  **Affiliation of breeders**   Aaron Beattie, University of Saskatchewan, Saskatoon, Saskatchewan
  **Breeding method**           pedigree
  **Use type**                  two row, general purpose spring barley
  **Pedigree**                  
  **Female parent**             
  Name:                         CDC Rattan
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         SH041242
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      'CDC Marlina' (experimental designations 'HB11316', 'SH080213') originated from the cross 'CDC Rattan' x 'SH041242' made in a greenhouse at the Crop Development Centre, University of Saskatchewan, Saskatoon, Saskatchewan in 2005 using a pedigree breeding system. Both the F1 and F2 generations were grown as bulk populations in a winter nursery in New Zealand in 2005 and in Saskatoon in 2006. During the winter of 2006 and 2007, both the F3 and F4 generations were grown as single seed derived lines. In 2007, 'CDC Marlina' was grown and selected from the 'SH080213' line which was bulked from seed selected from a F5 hill plot in Saskatoon. The variety was tested in the Crop Development Centre yield trials in 2008 to 2010 followed by testing in the Western Canadian Hulless Barley Registration Trials as 'HB11316' during 2011 and 2012. Selection criteria included yield, thresh-ability, earlier maturity, disease resistance and grain quality.
  **Pedigree tree**             
  **Performance**               fair resistance to lodging, good resistance to shattering, good tolerance to straw breakage, fair to good tolerance to drought
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/bar/app00009563e.shtml


