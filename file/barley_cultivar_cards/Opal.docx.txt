  Opal
  ------

  **Cultivar name**             Opal
  ----------------------------- -------------------------------------------------------------
  **Used names**                CI 6394
  **Origin**                    Denmark
  **Year released**             1929
  **Year started**              
  **Affiliation of breeders**   Abed Plant Breeding Station
  **Breeding method**           Pedigree
  **Use type**                  Two-Row, Spring
  **Pedigree**                  
  **Female parent**             
  Name:                         Binder
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         Gull
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               high yield potential
  **Reference**                 http://www.ars-grin.gov/cgi-bin/npgs/acc/display.pl?1133684


