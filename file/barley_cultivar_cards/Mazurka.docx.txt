  Mazurka
  ---------

  **Cultivar name**             Mazurka
  ----------------------------- -------------------------------------------------------------------------
  **Used names**                CI 15211
  **Origin**                    Netherlands
  **Year released**             1970
  **Year started**              
  **Affiliation of breeders**   Mansholt, R.J., Groniger Zaaizaadvereeniging
  **Breeding method**           Pedigree
  **Use type**                  Two-rowed spring barley
  **Pedigree**                  
  **Female parent**             
  Name:                         Hijlkema-1148
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         Heine-4808
  Origin:                       Germany
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 https://npgsweb.ars-grin.gov/gringlobal/accessiondetail.aspx?id=1264868


