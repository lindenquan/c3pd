Palliser

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Palliser
  ----------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                

  **Origin**                    Canada

  **Year released**             1960

  **Year started**              1951

  **Affiliation of breeders**   the Dryland Area Project Group of the Canada Department of Agriculture at Lethbridge, Albert

  **Breeding method**           Buck

  **Use type**                  Two-rowed spring barley

  **Pedigree**                  

  **Female parent**             

  Name:                         Vantage

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         Compana

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      Palliser originated from the cross Vantage/ Compana made in 1951. The objective was to produce a two-rowed variety with longer, stronger staw than Compana. The F1 and F3 generations were grown in bulk in California and the F2 and F4 at Lethbridge. Selection for plant and seed type was carried out in the F2 and F4, and the F5 was grown in progeny rows at Lethbridge in 1954. Barley infected with loose smut was sown in border rows on the windward side of the F4 and F5 material. Infection from this source was low, but very susceptible lines were infected by this natural inoculum and were eliminated. F5 lines were selected primarily for straw characteristics, resistance to shattering, seed type, and pearling quality. Uniform lines were bulked individually and tested in a preliminary yield test in 1955. Palliser was among this group and was included in regional test in at Swift Current, Scott, and Lethbridge in 1956. In 1957 it was advanced to the Co-operative Two-Rowed Barley Test for a 3-year period. On the Basis of its performance in these tests and in supplementary tests in Alberta and Saskatchewan in 1958 and 1959 it was licensed on April 19,1960.

  **Pedigree tree**             

  **Performance**               Its main attributes include superiority to Compana in yield, height, lodging resistance, and resistance to post-maturity stem break.

  **Reference**                 - S. A, Wells,
                                
                                Canada Agriculture Research Station,
                                
                                Lethbridge, Alberta
                                
                                - D. S. McBeaN,
                                
                                Experimental Farm,
                                
                                Swift Current, Saskatchewan
                                
                                -A. G. Kusch,
                                
                                Experimental Farm,
                                
                                Fort Vermilion, Alberta
                                
                                (formerly Experimental Farm,
                                
                                Scott. Saskatchewan)
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


