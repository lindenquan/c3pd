  DL 69
  -------

  -------------------------------------------------------------------------------------------------------
  **Cultivar name**             DL 69
  ----------------------------- -------------------------------------------------------------------------
  **Used names**                PI383854

  **Origin**                    

  **Year released**             

  **Year started**              

  **Affiliation of breeders**   

  **Breeding method**           Pedigree

  **Use type**                  6 row spring

  **Pedigree**                  

  **Female parent**             

  Name:                         BG1

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         Mexican 5-13

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      

  **Pedigree tree**             

  **Performance**               Semidwarf

  **Reference**                 <http://www.ars-grin.gov/cgi-bin/npgs/html/grin-acid.pl?DL70+19333+ped>
                                
                                http://genbank.vurv.cz/barley/pedigree/krizeni2.asp?id=172
  -------------------------------------------------------------------------------------------------------


