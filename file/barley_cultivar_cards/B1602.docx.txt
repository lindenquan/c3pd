  B1602
  -------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             B1602
  ----------------------------- ---------------------------------------------------------------------------------------------------------------------------
  **Used names**                BT922

  **Origin**                    Canada

  **Year released**             1989

  **Year started**              

  **Affiliation of breeders**   Busch Agricultural Resources, Inc

  **Breeding method**           Pedigree

  **Use type**                  6-row malting barley

  **Pedigree**                  

  **Female parent**             

  Name:                         Bumper / 6B78-628

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         Morex / 6B78-628

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      

  **Pedigree tree**             

  **Performance**               B1602 is being grown under contract by Agricore United for Anheuser-Busch, Inc. It has improved test weight over Bonanza.

  **Reference**                 <http://www.agric.gov.ab.ca/app95/loadCropVariety?action=display&id=54>
                                
                                <http://wheat.pw.usda.gov/cgi> bin/GG3/report.cgi?class=germplasm;name=B1602+(barley)
  ---------------------------------------------------------------------------------------------------------------------------------------------------------


