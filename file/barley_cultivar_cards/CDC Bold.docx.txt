  CDC Bold
  ----------

  **Cultivar name**             CDC Bold
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                SD422
  **Origin**                    Canada
  **Year released**             1999
  **Year started**              
  **Affiliation of breeders**   University of Saskatchewan
  **Breeding method**           Pedigree
  **Use type**                  2 row semi-dwarf feed barley
  **Pedigree**                  
  **Female parent**             
  Name:                         SB88404
  Origin:                       
  Use type:                     
  Improvement status:           Advanced breeding line
  Performance:                  
  **Male parent**               
  Name:                         Tyra
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               High yield potential, short strong straw, large plump grain with high test weight. It is resistant to the surface-borne smuts. Moderate resistance to scald, spot-form of net blotch and common root rot.
  **Reference**                 http://www.agric.gov.ab.ca/app95/loadCropVariety?action=display&id=25


