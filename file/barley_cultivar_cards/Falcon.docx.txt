  Falcon
  --------

  **Cultivar name**             Falcon
  ----------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                HB501
  **Origin**                    Canada
  **Year released**             1992
  **Year started**              
  **Affiliation of breeders**   Alberta Barley Development Group
  **Breeding method**           pedigree
  **Use type**                  six-rowed, hulless, semi-dwarf, spring-habit, feed barley
  **Pedigree**                  
  **Female parent**             
  Name:                         11012.2/Tern
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         Tulelake
  Origin:                       
  Use type:                     
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      Cultivar. Pureline. "FALCON"; HB 501. CV-253; PVP 9500315. Pedigree - 11012.2/Tern//Tulelake. Six-row, hulless feed type. Semidwarf, smooth awned, and yellow aleurone. Flag leaf dark green, medium wide, short, and semi-erect. Spike moderately dense, medium long, and of nodding attitude. Kernel medium wide and long. Rachilla long with long hairs. Maturity medium. Best adapted to central Alberta black soils. Yields 17% more than Condor, a two-row hulless cultivar. High digestible energy and protein for pigs. Resistant to lodging and scald (Rhynchosporium secalis).
  **Pedigree tree**             
  **Performance**               High scald and lodging
  **Reference**                 http://www.agric.gov.ab.ca/app95/loadCropVariety?action=display&id=42


