  Weihenstephan Mehltauresistente II
  ------------------------------------

  **Cultivar name**             Weihenstephan Mehltauresistente II
  ----------------------------- -------------------------------------------------------------
  **Used names**                HOR 110
  **Origin**                    Germany
  **Year released**             1945
  **Year started**              
  **Affiliation of breeders**   Inst.fur Pflanzenzuchtung u. Pflanzenbau Weihenstephan
  **Breeding method**           Pedigree
  **Use type**                  Two-row spring barley
  **Pedigree**                  
  **Female parent**             
  Name:                         Weihenstephan Mehltauresistente I
  Origin:                       Germany 1935
  Use type:                     Two-row spring barley
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Isaria (CI 7595)
  Origin:                       Germany 1924
  Use type:                     Two-row spring barley
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.ars-grin.gov/cgi-bin/npgs/acc/display.pl?1057843


