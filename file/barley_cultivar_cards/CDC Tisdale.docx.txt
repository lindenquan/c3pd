  CDC Tisdale
  -------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             CDC Tisdale
  ----------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                BT462

  **Origin**                    Canada

  **Year released**             2001

  **Year started**              

  **Affiliation of breeders**   University of Saskatchewan, Saskatoon

  **Breeding method**           pedigree

  **Use type**                  6-row malt barley

  **Pedigree**                  

  **Female parent**             

  Name:                         BT409

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Male parent**               

  Name:                         Foster

  Origin:                       

  Use type:                     

  Improvement status:           

  Performance:                  

  **Pedigree**                  

  **Pedigree description**      

  **Pedigree tree**             

  **Performance**               Good yield, kernel weight, plumpness and straw strength than checks B1602 and CDC Sisler. It has very good overall malting quality with low beta-glucan. It has resistance to the surface-borne smuts and intermediate resistance to common root rot and net blotch. CDC Tisdale is currently under market development, but is not yet being grown for commercial production.

  **Reference**                 <http://www.agric.gov.ab.ca/app95/loadCropVariety?action=display&id=626>
                                
                                https://triticeaetoolbox.org/barley/pedigree/pedigree\_tree.php?line=BT462
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


