**Dutch White Blossom**

  **Cultivar name**             Dutch White Blossom
  ----------------------------- ---------------------
  **Used names**                
  **Origin**                    
  **Year registered**           1940
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Use type**                  

  **Pedigree**   
  -------------- --

  --------------------------------------------------------------
  **Female parent**   **Male parent**
  ------------------- ----------------- --------------------- --
  Name:               

  Origin:             

  Use type:           

  Improvement         
                      
  status:             

  Performance:        
  --------------------------------------------------------------

  **Pedigree description**   
  -------------------------- --

  **Performance**          
  ------------------------ --
  **Disease resistance**   
  **References**           


