**CDC Gold**

  **Cultivar name**             CDC Gold
  ----------------------------- -----------------------------------------------------
  **Used names**                SP2100
  **Origin**                    Canada
  **Year registered**           Abandoned
  **Year started**              
  **Affiliation of breeders**   Crop Development Centre, University of Saskatchewan
  **Breeding method**           
  **Use type**                  Oil

  **Pedigree**   ED-48/YSED-19 or McGregor//McGregor/1747/3/1747/CP84495
  -------------- ---------------------------------------------------------

  ---------------------------------------------------------------------
  **Female parent**   **Male parent**
  ------------------- ----------------- --------------------- ---------
  Name:               ED-48

  Origin:             Canada

  Use type:           

  Improvement         
                      
  status:             

  Performance:        
  ---------------------------------------------------------------------

  -------------------------------------------------------------------------
  **Pedigree description**   ED-48's pedigree is McGregor//McGregor/1747.
                             
                             YSED-19's pedigree is 1747/CP84495.
  -------------------------- ----------------------------------------------
  -------------------------------------------------------------------------

  **Performance**          
  ------------------------ ----------------------------------------------------------------
  **Disease resistance**   
  **Reference**            Information provided by Helen Booker, Crop Development Centre.


