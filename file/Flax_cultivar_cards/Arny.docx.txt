**Arny**

  **Cultivar name**             Arny
  ----------------------------- -------------------------------------------
  **Used names**                
  **Origin**                    US
  **Year registered**           1961
  **Year started**              
  **Affiliation of breeders**   Minnesota Agricultural Experiment Station
  **Breeding method**           Pedigree
  **Use type**                  Oil

  **Pedigree**   Crystal/Redson
  -------------- ----------------

  -------------------------------------------------------------------------------------------------
  **Female parent**   **Male parent**
  ------------------- -------------------------------------------- --------------------- ----------
  Name:               Crystal

  Origin:             US

  Use type:           

  Improvement         Cultivar
                      
  status:             

  Performance:        Resistant to rust. Semi-resistant to wilt.
  -------------------------------------------------------------------------------------------------

  **Pedigree description**   
  -------------------------- --

  -------------------------------------------------------------------------------------------------------------------------------------------
  **Performance**          Late maturity. Good oil content and quality.
  ------------------------ ------------------------------------------------------------------------------------------------------------------
  **Disease resistance**   Immune to North American rust, resistant to fusarium wilt, and tolerant to pasmo.

  **References**           Arny, A. C. "Flax Varieties Registered, I." *Agronomy Journal* 35, no. 9 (1943): 823-824.
                           
                           Culbertson, J. O. "Registration of Improved Flax Varieties, Viii." *Agronomy Journal* 53, no. 6 (1961): 401-402.
  -------------------------------------------------------------------------------------------------------------------------------------------


