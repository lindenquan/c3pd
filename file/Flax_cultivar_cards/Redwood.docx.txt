**Redwood**

  ----------------------------------------------------------------------------------------------------
  **Cultivar name**             Redwood
  ----------------------------- ----------------------------------------------------------------------
  **Used names**                C.I. 1130

  **Origin**                    US

  **Year registered**           1951

  **Year started**              

  **Affiliation of breeders**   Minnesota Agricultural Experiment Station;
                                
                                Division of Cereal Crops and Diseases, US Department of Agriculture.

  **Breeding method**           Pedigree

  **Use type**                  Oil
  ----------------------------------------------------------------------------------------------------

  **Pedigree**   B-5128/Redson
  -------------- ---------------

  ----------------------------------------------------------------------
  **Female parent**   **Male parent**
  ------------------- ------------------- --------------------- --------
  Name:               B-5128

  Origin:             US

  Use type:           Oil

  Improvement         Cultivar
                      
  status:             

  Performance:        Resistant to rust
  ----------------------------------------------------------------------

  **Pedigree description**   
  -------------------------- --

  **Performance**          Good oil content with acceptable quality.
  ------------------------ ------------------------------------------------------------------------------------------------------------------
  **Disease resistance**   Immune to rust, and resistant to fusarium wilt.
  **References**           Culbertson, J. O. "Registration of Improved Varieties of Flax, V." *Agronomy Journal* 44, no. 3 (1952): 153-154.


