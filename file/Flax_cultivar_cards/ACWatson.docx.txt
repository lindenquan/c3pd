**AC Watson**

  **Cultivar name**             AC Watson
  ----------------------------- ----------------------------------------------------------
  **Used names**                M2859, FP974
  **Origin**                    Canada
  **Year registered**           1997
  **Year started**              1985
  **Affiliation of breeders**   Morden Research Centre, Agriculture and Agri-Food Canada
  **Breeding method**           Pedigree
  **Use type**                  Oil

  **Pedigree**   NorLin//FP775/C.I. 2941
  -------------- -------------------------

  ------------------------------------------------------------------------------------------------------------------------------
  **Female parent**   **Male parent**
  ------------------- ------------------------------------------------------------------ --------------------- -----------------
  Name:               NorLin

  Origin:             Canada

  Use type:           Oil

  Improvement         Cultivar
                      
  status:             

  Performance:        Medium oil content. Good oil quality. Resistant to rust and wilt
  ------------------------------------------------------------------------------------------------------------------------------

  **Pedigree description**   FP775 originated form the cross McGregor//Redwood 65/a high oil line of unknown origin. CI2941 originated from the cross Culbert/5058, a line of unknown origin. Using the pedigree method, F2 and subsequent generations were advanced to F7 generation. An F7 line, M2859, was selected for early maturity, large seed size, high oil content and quality, and rust resistance.
  -------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Performance**          Medium-early maturity, high oil quality, medium-large seed size, good lodging resistance. Good yielding in Black and Brown Soil zones.
  ------------------------ ----------------------------------------------------------------------------------------------------------------------------------------
  **Disease resistance**   Resistant to North American races of rust and moderately resistant to wilt.

  **Reference**            Kenaschuk, E. O. and K. Y. Rashid. "Ac Watson Flax." *Canadian Journal of Plant Science* 78, no. 3 (1998): 465-466.
                           
                           Kenaschuk, Edward O. and John A. Hoes. "Norlin Flax." *Canadian Journal of Plant Science* 66, no. 1 (1986): 171-173.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------


