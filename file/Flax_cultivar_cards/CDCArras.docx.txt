**CDC Arras**

  -----------------------------------------------------------------------------------------
  **Cultivar name**             CDC Arras
  ----------------------------- -----------------------------------------------------------
  **Used names**                FP1171, FP1030

  **Origin**                    Canada

  **Year registered**           1998

  **Year started**              1985

  **Affiliation of breeders**   Crop Development Centre, University of Saskatchewan;
                                
                                Agriculture and Agri-Food Canada, Morden Research Station

  **Breeding method**           Pedigree

  **Use type**                  Oil
  -----------------------------------------------------------------------------------------

  **Pedigree**   Vimy/FP833
  -------------- ------------

  -------------------------------------------------------------------------------------------------------------------------------
  **Female parent**   **Male parent**
  ------------------- --------------------------------------------------------------------- --------------------- ---------------
  Name:               Vimy

  Origin:             Canada

  Use type:           Oil

  Improvement         Cultivar
                      
  status:             

  Performance:        High yield. High oil content + quality. Resistant to rust and wilt.
  -------------------------------------------------------------------------------------------------------------------------------

  **Pedigree description**   FP833 is a reselection of Noralta M^6^ from the Morden Research Station. Using the pedigree method, F2 and subsequent generations were advanced to the F6. An F7 line, F91171, was selected for early maturing, high oil content, high oil quality and rust resistance.
  -------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Performance**          Medium-early-maturing oilseed. Medium oil content with good oil quality, large seed size. Fair lodging resistance. High yield in Black and Brown Soil zones,
  ------------------------ --------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Disease resistance**   Resistant to North American races of rust and moderately resistant to wilt.

  **Reference**            Rowland, G. G. and R. S. Bhatty. "Vimy Flax." *Canadian Journal of Plant Science* 67, no. 1 (1987): 245-247.
                           
                           Rowland, G. G., Y. A. Hormis and K. Y. Rashid. "Cdc Arras Flax." *Canadian Journal of Plant Science* 82, no. 1 (2002): 99-100.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


