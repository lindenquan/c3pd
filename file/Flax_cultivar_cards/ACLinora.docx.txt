**AC Linora**

  **Cultivar name**             AC Linora
  ----------------------------- -----------------------------------------------------------
  **Used names**                M1353, FP862
  **Origin**                    Canada
  **Year registered**           1991
  **Year started**              1978
  **Affiliation of breeders**   Agriculture and Agri-Food Canada, Morden Research Station
  **Breeding method**           Pedigree
  **Use type**                  Oil

  **Pedigree**   Linott/NorMan
  -------------- ---------------

  ----------------------------------------------------------------------------------------------------------------------------------------
  **Female parent**   **Male parent**
  ------------------- ----------------------------- --------------------- ----------------------------------------------------------------
  Name:               Linott

  Origin:             Canada

  Use type:           Oil

  Improvement         Cultivar
                      
  status:             

  Performance:        Resistant to rust and wilt.
                      
                      High Yield
  ----------------------------------------------------------------------------------------------------------------------------------------

  **Pedigree description**   Using the pedigree method, F1 generation were advanced to F7. Single selections were made in F3 and F5 generations. An F7 line, M1353, was selected.
  -------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Performance**          Medium-early maturing oilseed flax, high oil content and quality. High yield in early and late seeding,
  ------------------------ ----------------------------------------------------------------------------------------------------------------------------------------------------
  **Disease resistance**   Resistant to North American races of rust and moderately resistant to wilt.

  **Reference**            Kenaschuk, E. O. and K. Y. Rashid. "Ac Linora Flax." *Canadian Journal of Plant Science* 73, no. 3 (1993): 839-841.
                           
                           Kernaschuk, E. O. "Registration of Linott Flax1 (Reg. No. 34)." *Crop Science* 20, no. 2 (1980): 286-286.
                           
                           Rowland, G. G., A. G. McHughen, Y. A. Hormis and K. Y. Rashid. "Cdc Normandy Flax." *Canadian Journal of Plant Science* 82, no. 2 (2002): 425-426.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


