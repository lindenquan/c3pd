**Cascade**

  **Cultivar name**             Cascade
  ----------------------------- ---------
  **Used names**                
  **Origin**                    
  **Year registered**           1952
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Use type**                  

  **Pedigree**   
  -------------- --

  --------------------------------------------------------------
  **Female parent**   **Male parent**
  ------------------- ----------------- --------------------- --
  Name:               

  Origin:             

  Use type:           

  Improvement         
                      
  status:             

  Performance:        
  --------------------------------------------------------------

  **Pedigree description**   
  -------------------------- --

  **Performance**          
  ------------------------ --
  **Disease resistance**   
  **References**           


