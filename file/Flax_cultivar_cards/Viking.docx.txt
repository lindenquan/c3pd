**Viking**

  **Cultivar name**             Viking
  ----------------------------- ---------------------------------
  **Used names**                
  **Origin**                    US
  **Year registered**           1945
  **Year started**              
  **Affiliation of breeders**   North Dakota Experiment Station
  **Breeding method**           
  **Use type**                  Oil

  **Pedigree**   
  -------------- --

  ------------------------------------------------------------------------------
  **Female parent**   **Male parent**
  ------------------- ----------------- --------------------- ------------------
  Name:               Bolley’s Golden

  Origin:             

  Use type:           

  Improvement         
                      
  status:             

  Performance:        
  ------------------------------------------------------------------------------

  **Pedigree description**   
  -------------------------- --

  **Performance**          Mid-late maturity. Good yield.
  ------------------------ ---------------------------------------------------------------------------------------------------------------------------------------------------
  **Disease resistance**   Immune to rust, and resistant to wilt.
  **References**           Flax Agronomic Descriptions, North Dakota Agricultural Experiment Station. <Http://Www.Ag.Ndsu.Nodak.Edu/Aginfo/Seedstock/Varieties/Va-Flax.Htm.>


