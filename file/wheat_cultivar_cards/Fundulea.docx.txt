**Fundulea**

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Fundulea
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                F 29-76

  **Origin**                    Romania

  **Year released**             1991 (applied, abandoned 1997)

  **Year started**              \~1979

  **Affiliation of breeders**   Pflanzenzucht Oberlimpurg, Germany; ICCPT, Romania; C&M Seeds; Charlottetown Research Station (AAFC); Harrow Research Station (AAFC); Plant Research Centre (AAFC); Kentville Research Station (AAFC); Nova Scotia Agricultural College; Fredericton Research Station (AAFC); Minas Seed Co-op Ltd.

  **Breeding method**           Pedigree or bulk????

  **Type**                      Winter wheat, hard red

  **Pedigree**                  

  **Female parent**             

  Name:                         Sava/Kavkas

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Male parent**               

  Name:                         Bezostaja 1

  Origin:                       

  Type:                         

  Improvement status:           Cultivar

  Performance:                  

  **Pedigree**                  Sava/Kavkas/2/Bezostaja 1

  **Pedigree description**      

  **Pedigree tree**             

  **Performance**               Bread-making quality and high grain yield. It is moderately susceptible to powdery mildew and septoria leaf and glume blotch and is suited for
                                
                                production in areas of Atlantic Canada where winter survival is not a problem

  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00000192e.shtml
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


