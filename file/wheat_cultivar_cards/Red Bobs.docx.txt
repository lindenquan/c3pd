**Red Bobs**

  **Cultivar name**             Red Bobs
  ----------------------------- ------------------------------
  **Used names**                Early Triumph, CT 008, TD 10
  **Origin**                    Canada
  **Year released**             1918
  **Year started**              
  **Affiliation of breeders**   Seager Wheeler
  **Breeding method**           Selection
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Bobs
  Origin:                       Australia
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  Bobs
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


