**AAC Penhold**

  **Cultivar name**             AAC Penhold
  ----------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                HY1319, C0401-HH45E
  **Origin**                    Canada
  **Year released**             2013 (applied, pending)
  **Year started**              2004
  **Affiliation of breeders**   Agriculture & Agri-Food Canada
  **Breeding method**           Pedigree?
  **Type**                      Spring wheat, common medium red
  **Pedigree**                  
  **Female parent**             
  Name:                         5700PR/HY644-BE
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         HY469
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  5700PR/HY644-BE//HY469
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Resistant to Leaf Rust (Puccinia triticina), resistant to moderately resistant to Stem Rust (Puccinia graminis f. sp. tritici) and Common Bunt (Tilletia laevis and T. tritici), moderately resistant to Fusarium Head Blight (Fusarium graminearum, Fusarium species) and moderately resistant to moderately susceptible to Loose Smut (Ustilago tritici)
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00009280e.shtml


