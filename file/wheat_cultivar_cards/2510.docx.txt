**2510**

  **Cultivar name**             2510
  ----------------------------- --------------------------------
  **Used names**                
  **Origin**                    US
  **Year released**             1995 (applied, abandoned 1999)
  **Year started**              
  **Affiliation of breeders**   Pioneer Hi-Bred Ltd
  **Breeding method**           
  **Type**                      
  **Pedigree**                  
  **Female parent**             
  Name:                         WBGO195E2
  Origin:                       
  Use type:                     
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         2510
  Origin:                       
  Use type:                     
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  WBGO195E2/2510
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


