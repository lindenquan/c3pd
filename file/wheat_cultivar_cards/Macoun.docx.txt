**Macoun**

  **Cultivar name**             Macoun
  ----------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                DT332
  **Origin**                    Canada
  **Year released**             1974
  **Year started**              1964
  **Affiliation of breeders**   Winnipeg Research station (AAFC); Swift Current Research Station (AAFC)
  **Breeding method**           
  **Type**                      Durum, amber
  **Pedigree**                  
  **Female parent**             
  Name:                         RL3607
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         DT182
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  RL3607/DT182
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Compared to Wascana, it is shorter, stronger strawed, earlier maturing, higher hectoliter weight and is equal in disease resistance, yields slightly more in the Black soil zone and is about equal in the Brown soil zone, medium early maturity, resistant to loose smut (Ustilago tritici \[Pers.\] Rostr.), bunt (Tilletia foetida \[Wallr.\] Liro), leaf rust (Puccinia recondite Rob. ex. Desm,. f. sp. tritici) and stem rust {Puccinia graminis Pers. f. sp. tritici Eriks. and E. Henn.) races prevalent, moderately resistant to kernel smudge, equal to Wascana and Hercules to common root rot (Bipolaris sorokiniana \[Sacc. In Sorok.\] Shoem. And Fusarium sp.)
  **Reference**                 


