**Alikat**

  **Cultivar name**             Alikat
  ----------------------------- -------------------------------
  **Used names**                PT 755
  **Origin**                    Canada
  **Year released**             1999
  **Year started**              
  **Affiliation of breeders**   University of Alberta
  **Breeding method**           
  **Type**                      Spring wheat, red (CWRS)
  **Pedigree**                  
  **Female parent**             
  Name:                         Katepwa\*3
  Origin:                       Canada
  Type:                         Spring wheat, hard red (CWRS)
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Maringa
  Origin:                       Brazil
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Katepwa\*3/Maringa
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


