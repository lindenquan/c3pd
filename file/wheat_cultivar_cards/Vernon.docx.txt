**Vernon**

  **Cultivar name**             Vernon
  ----------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                MRO-1
  **Origin**                    Canada
  **Year released**             Not licensed
  **Year started**              
  **Affiliation of breeders**   Atlantic Provinces Field Crops Committee; Charlottetown Research Station (AAFC); Fredericton Research Station (AAFC); Nappan Experimental Farm; Nova Scotia Agricultural College
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Opal\*4
  Origin:                       Germany
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Pompe
  Origin:                       Sweden
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Opal\*4/Pompe
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Higher yielding than recommended cultivars in the Atlantic Provinces and is resistant to Erysiphe graminis in this area, moderate shattering, moderately resistant to lodging, resistant to powdery mildew in the Maritime provinces, susceptible to leaf rust (Puccinia recondite Rob. ex. Desm.) and Septoria nodorum Berk. And loose smut (Ustilago tritici (Pers.) Rostr.), maturity similar to Opal, test weight satisfactory slightly greater than Opal, Kernal weight satisfactory 4% greater than Opal
  **Reference**                 


