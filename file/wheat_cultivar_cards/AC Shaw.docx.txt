**Shaw**

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Shaw
  ----------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                Pilot, BA51

  **Origin**                    Canada

  **Year released**             2011

  **Year started**              2001

  **Affiliation of breeders**   Cereal Research Centre (AAFC); Office of Intellectual Property and Commercialization (AAFC); SemiArid Prairie Agricultural Research Centre (AAFC); Lethbridge Research Centre (AAFC)

  **Breeding method**           Double haploid

  **Type**                      Spring wheat, hard red (Canada Western Red Spring)

  **Pedigree**                  

  **Female parent**             

  Name:                         Harvest

  Origin:                       Canada

  Type:                         Spring wheat, hard red

  Improvement status:           Cultivar

  Performance:                  

  **Male parent**               

  Name:                         BW313

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Pedigree**                  Harvest(BW259)/BW313(RL4979)

  **Pedigree description**      Harvest has the parentage AC Domain\*2/ND640
                                
                                BW313 has the parentage RL4763\*2/Howell.
                                
                                RL4763=Roblin\*4/BW553

  **Pedigree tree**             

  **Performance**               Resistant to Leaf rust (Puccinia triticina), Stem rust (Puccinia graminia f.sp.tritici) and Common bunt (Tilletia caries, Tilletia foetida), susceptible to Loose smut (Ustilago tritici) and Fusarium head blight (Fusarium graminearum, Fusarium species), resistant to Orange Blossom Wheat Midge (Sitodiplosis mosellana), good resistance to shattering, good bread making quality, contains the antibiosis resistance gene Sm1, which is effective against the wheat midge larvae (Sitodiplosis mosellana Gehin), adapted to the eastern wheat growing regions of the Canadian prairies, highest yielding cultivar overall; although not significant, Shaw was 1% higher yielding than
                                
                                Unity VB. Shaw matured significantly earlier than 5603HR. The plant stature of Shaw was significantly taller than all of the checks except Katepwa, but had similar lodging resistance scores as the checks. The test weight of Shaw was significantly higher than Katepwa and CDC Teal and within the range of the other checks

  **Reference**                 <http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00007657e.shtml>
                                
                                Agronomix Software Inc. 2009. Agrobase Generation II. Version 18. Agronomix Software Inc., Winnipeg, MB.
                                
                                American Association of Cereal Chemists. 2002. Approved methods of the AACC. 10th ed. The Association, St. Paul, MN.
                                
                                Barker, P. S. and McKenzie, R. I. H. 1996. Possible sources of resistance to the wheat midge in wheat. Can. J. Plant Sci. 76: 689695.
                                
                                Black, H. C., Hsieh, F. H., Tipples, K. H. and Irvine, G. N. 1980. GRL sifter for laboratory flour milling. Cereal Foods World 25: 757760.
                                
                                Campbell, A. B. and Czarnecki, E. 1987. Katepwa hard red spring wheat. Can. J. Plant Sci. 67: 229230.
                                
                                Dexter, J. E. and Tipples, K. H. 1987. Wheat milling at the Grain Research Laboratory. Part 3. Effect of grading factors on wheat quality. Milling 180: 16, 1820.
                                
                                Fetch, T. G. 2005. Races of Puccinia graminis on wheat, barley, and oat in Canada, in 2002 and 2003. Can. J. Plant Pathol. 27: 572580.
                                
                                Fox, S. L., McKenzie, R. I. H., Lamb, R. J., Wise, I. L., Smith, M. A. H., Humphreys, D. G., Brown, P. D., Townley-Smith, T. F., McCallum, B. D., Fetch, T. G., Menzies, J. G., Gilbert, J. A., Fernandez, M. R., Despins, T., Lukow, O. and Niziol, D. 2010a. Unity hard red spring wheat. Can. J. Plant Sci. 90: 7178.
                                
                                Fox, S. L., Townley-Smith, T. F., Thomas, J. B., Humphreys, D. G., Brown, P. D., McCallum, B. D., Fetch, T. G., Menzies, J. G., Gilbert, J. A., Fernandez, M. R., Gaudet, D. A. and Noll, J. S. 2010b. Harvest hard red spring wheat. Can. J. Plant Sci. 90: 503509.
                                
                                Gaudet, D. A. and Puchalski, B. L. 1989. Races of common bunt (Tilletia caries and T. foetida) in western Canada. Can. J. Plant Pathol 11: 415418.
                                
                                Gaudet, D. A., Puchalski, B. L., Schallje, G. B. and Kozub, G. C. 1993. Susceptibility and resistance in Canadian spring wheat cultivars to common bunt (Tilletia tritici and T. laevis). Can. J. Plant Sci. 69: 797804.
                                
                                Gilbert, J. and Woods, S. 2006. Strategies and considerations for multi-location FHB screening nurseries. Pages 93102 in
                                
                                T. Ban, J. M. Lewis, and E. E. Phipps, eds. The global fusarium initiative for international collaboration: A strategic planning workshop. CIMMYT, El Bata\` n, Mexico; 2006 Mar. 1417. Cross-ref Publication No. 7. CIMMYT, Mexico, D.F.
                                
                                Graf, R. J., Hucl, P., Orshinsky, B. R. and Kartha, K. K. 2003. McKenzie hard red spring wheat. Can. J. Plant Sci. 83: 565569.
                                
                                Hughes, G. R. and Hucl, P. J. 1993. CDC Teal hard red spring wheat. Can. J. Plant Sci. 73: 193197.
                                
                                Humphreys, D. G. and Noll, J. 2002. Methods for characterization of preharvest sprouting resistance in a wheat breeding program. Euphytica 126:6165.
                                
                                Kolb, F. L. and Brown, C. M. 1992. Registration of ‘Howell’ Wheat. Crop Sci. 32: 1292.
                                
                                McCaig, T. N., DePauw, R. M., McLeod, J. G., Knox, R. E., Clarke, J. M. and Fernandez, M. R. 1996. AC Barrie hard red spring wheat. Can. J. Plant Sci. 76: 337339.
                                
                                McCaig, T. N., DePauw, R. M., Clarke, J. M., McLeod, J. G., Fernandez, M. R. and Knox, R. E. 1996. AC Barrie hard red spring wheat. Can. J. Plant Sci. 76: 337339.
                                
                                McKenzie, R. I. H., Lamb, R. J., Aung, T., Wise, I. L., Barker, P. and Olfert, O. O. 2002. Inheritance of resistance to wheat midge, Sitodiplosis mosellana, in spring wheat. Plant Breed. 121: 383388.
                                
                                McCallum, B. D. and Seto-Goh, P. 2009. Physiologic specialization of Puccinia triticina, the causal agent of wheat leaf, in Canada in 2006. Can. J. Plant Pathol. 31:8087.
                                
                                Menzies, J. G., Knox, R. E., Nielsen, J. and Thomas, P. L. 2003. Virulence of Canadian isolates of Ustilago tritici; 19641998, and the use of the geometric rule in understanding host differential complexity. Can. J. Plant Pathol. 25:6272.
                                
                                Peterson, R. F., Campbell, A. B. and Hannah, A. E. 1948. A diagramatic scale for estimating rust intensity on leaves and stems of cereals. Can. J. Res. 26: sec C: 496500.
                                
                                Preston, K. R., Kilborn, R. H. and Black, H. C. 1982. The GRL pilot mill. II. Physical dough and baking properties of flour streams milled from Canadian red spring wheats. Can. Inst. Food Sci. Technol. J. 15:2936.
                                
                                Roelfs, A. P. and Martens, J. W. 1988. An international system of nomenclature for Puccinia graminis f. sp. Tritici. Phytopathology 78: 526533.
                                
                                SAS Institute, Inc. 2006. SAS software. Version 9.13. SAS Institute, Inc., Cary, NC. Smith, M. A. H., Lamb, R. J., Wise, I. L. and Olfert, O. O.
                                
                                2004\. An interspersed refuge for Sitodiplosis mosellana (Diptera:
                                Cecidomyiidae) and a biocontrol agent Macroglenes penetrans
                                (Hymenoptera: Pteromalidae) to manage crop resistance in wheat. Bull.
                                Entomol. Res. 94: 179188.
                                
                                Thomas, J., Fineberg, N., Penner, G., McCartney, C., Aung, T., Wise, I. and McCallum, B. 2005. Chromosome location and markers of Sm1: a gene of wheat that conditions antibiotic resistance to orange wheat blossom midge. Mol. Breed. 15: 183192.
                                
                                Townley-Smith, T. F. and Czarnecki, E. M. 2008. AC Domain hard red spring wheat. Can. J. Plant Sci. 88: 347350.
                                
                                Williams, P., Sobering, D. and Antoniszyn, J. 1998. Protein testing methods at the Canadian Grain Commission. In Wheat Protein Symposium: proceedings; 1998 March 910. Saskatoon, SK
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


