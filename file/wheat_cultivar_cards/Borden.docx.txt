**Borden**

  **Cultivar name**             Borden
  ----------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                CH75-176-2, CH1B
  **Origin**                    Canada
  **Year released**             1983
  **Year started**              1973
  **Affiliation of breeders**   Charlottetown Research Station (AAFC); Fredericton Research Station (AAFC); Kentville Research Station (AAFC); Nova Scotia Agricultural College
  **Breeding method**           Pedigree?
  **Type**                      Winter wheat, medium-hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         Genesee
  Origin:                       US
  Type:                         Winter wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Lennox
  Origin:                       US
  Type:                         Winter wheat, hard red
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Genesee/Lennox
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Good winterhardiness and high yield in the Atlantic Provinces of Canada, moderately resistant to powdery mildew (Erysiphe graminis, D.C. ex. Merat f. sp. Tritici Marchal) and Septoria (Septoia nodorum Berk.); resistant to pink snow mold (Gerlachia nivalis (Ges. Ex Sacc.) W. Gams and E. Muller), does not sprout readily prior to harvest, resistant to shattering, medium maturity
  **Reference**                 


