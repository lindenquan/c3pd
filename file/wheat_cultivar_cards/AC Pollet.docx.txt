**AC Pollet**

  **Cultivar name**             AC Pollet
  ----------------------------- -------------------------------
  **Used names**                Q.W.500.45
  **Origin**                    Canada
  **Year released**             1995
  **Year started**              
  **Affiliation of breeders**   SeCan Association
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Frontana-LF-320
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         Maderes/Opal
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Frontana-LF-320//Maderes/Opal
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


