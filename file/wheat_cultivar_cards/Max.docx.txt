**Max**

  **Cultivar name**             Max
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                
  **Origin**                    Germany
  **Year released**             1986
  **Year started**              \~1978
  **Affiliation of breeders**   Minas Seed Cooperative Limited; Saatzucht, Germany; Charlottetown Research Station (AAFC); Kentville Research Station (AAFC); Nova Scotia Agricultural College; Fredericton Research Station (AAFC); Nova Scotia Experimental Farm; Senator Michaud Experimental Farm
  **Breeding method**           
  **Type**                      Spring wheat, hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         Sappo
  Origin:                       
  Type:                         
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Kolibri
  Origin:                       
  Type:                         
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Sappo/Kilibri
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Milling quality and high yield in Eastern Canada, resistant to powdery mildew has good straw strength, and is suited for production under intensive management, resistant to powdery mildew caused by Erysiphe graminis D.C. ex. Merat f. sp. tritici Marchal, highly susceptible to loose smut caused by Ustilago tritici (Pers.) Ronstr., moderately susceptible to leaf rust caused byPuccinia recondite Rob. ex. Desm. f. sp. tritici, head blight caused by Fuasrium spp., and Septoria leaf and glume blotch caused by Septoria nodorum Berk.
  **Reference**                 


