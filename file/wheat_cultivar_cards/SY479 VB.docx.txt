**SY479 VB**

  **Cultivar name**             SY479 VB
  ----------------------------- -------------------------------------------------------------------------------------
  **Used names**                BW479
  **Origin**                    Canada
  **Year released**             2014 (applied, pending)
  **Year started**              
  **Affiliation of breeders**   Syngenta Canada
  **Breeding method**           
  **Type**                      
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00009605e.shtml


