**HY 300-HRW**

  **Cultivar name**             HY 300-HRW
  ----------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                TW 300-001
  **Origin**                    Canada?
  **Year released**             2014
  **Year started**              1999-2000
  **Affiliation of breeders**   Agrigenetics, Inc. (A division of Dow AgroSciences Inc.), US; Hyland Seeds (A Division of Dow AgroSciecnes, Inc.); Smart & Biggar
  **Breeding method**           Double haploid maize hybridization
  **Type**                      Winter wheat, common hard red (CEHRW)
  **Pedigree**                  
  **Female parent**             
  Name:                         Waldorf
  Origin:                       Belgium
  Type:                         Winter wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         TW 97407
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Waldorf/TW 97407
  **Pedigree description**      Waldorf= Fundulea/Karl-92 and TW 97407 = Ruby/Semu1841
  **Pedigree tree**             
  **Performance**               Resistant to Black Point & smudge (Cochliobolus sativa, Alternaria species, Pseudomonas syringae pv. atrofaciens) and Barley Yellow Dwarf virus, resistant to moderately resistant to Leaf rust (Puccinia triticina), moderately resistant to Spindle streak mosaic virus, moderately resistant to moderately susceptible to Septoria tritici blotch (Septoria tritici) and Stripe rust (Puccinia striiformis) and moderately susceptible to Fusarium head blight (Fusarium graminearum, Fusarium species), good winter survival
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00008303e.shtml


