**Cascade**

  **Cultivar name**             Cascade
  ----------------------------- --------------------------------------------
  **Used names**                CAN 3593
  **Origin**                    Canada
  **Year released**             1947
  **Year started**              
  **Affiliation of breeders**   Central Experimental Farm
  **Breeding method**           
  **Type**                      Spring wheat, hard white
  **Pedigree**                  
  **Female parent**             
  Name:                         Quality A/Pacific Bluestem//C26592D
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         Onas
  Origin:                       Australia
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Quality A/Pacific Bluestem//C26592D/3/Onas
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


