**Monopol**

  **Cultivar name**             Monopol
  ----------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                
  **Origin**                    Germany
  **Year released**             1975 (Germany) 1982 (Canada)
  **Year started**              
  **Affiliation of breeders**   Minas Seed Cooperative Limited; Charlottetown Research Station (AAFC); Nova Scotia Department of Agriculture and Marketing, Kentville; Nova Scotia Department of Agriculture and Marketing, Truro; Nappan Experimental Farm; New Brunswick Department of Agriculture and Rural Development
  **Breeding method**           Selection?
  **Type**                      Winter wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Panthus
  Origin:                       
  Type:                         
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Admiral
  Origin:                       
  Type:                         
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Panthus/Admiral
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Moderately resistant to powdery mildew (Erysiphe graminis, P.C. ex. Merat f. sp. tritici Marchal), moderately susceptible to pink snow mold (Gerlachia nivalis Ges ex Sacc) W. Gams and E. Muller, and moderately resistant to Septoria (Septoria nodorum Berk.), resistant to shattering, seed does not sprout readily prior to harvest, late maturity
  **Reference**                 


