**Coleman**

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Coleman
  ----------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                

  **Origin**                    Canada

  **Year released**             2013

  **Year started**              2004-2005

  **Affiliation of breeders**   Agricultural, Food & Nutritional Science, University of Alberta; Plant Agriculture, Ontario Agricultural College, University of Guelph; National Agricultural Research Centre; Lethbridge Research Centre (AAFC)

  **Breeding method**           Single seed descent

  **Type**                      Spring wheat, hard red (Canada Western Red Spring)

  **Pedigree**                  

  **Female parent**             

  Name:                         CDC Go

  Origin:                       Canada

  Type:                         CWRS

  Improvement status:           Cultivar

  Performance:                  

  **Male parent**               

  Name:                         FRTL/NEMURA//McKenzie

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Pedigree**                  CDC Go/3/FRTL/NEMURA//McKenzie

  **Pedigree description**      FRTL stands for Firetail, which is derived from “Fong Chan 3/Tyrant//Veery \#9”, while NEMURA derives from the cross “Neuzucht/Bezostaya 1//Alondra/4/Nadadores 63//Triumph/CI12406/3/Emu”

  **Pedigree tree**             

  **Performance**               Well adapted to the wheat growing regions of Western Canada, higher yielding than Katepwa, AC Splendor, CDC Teal, and CDC Osler, exhibited maturity, height and lodging resistance similar to, or in the range of the checks, had higher test weights than the checks and showed good resistance to leaf, stem and stripe rust, exhibited Fusarium head blight resistance greater than and DON contamination levels lower than the check cultivars, exhibited susceptible reactions to common bunt and loose smut

  **Reference**                 American Association of Cereal Chemists. 2000. Approved methods of the AACC. 10th ed. AACC, St. Paul, MN.
                                
                                Fox, S. L. and McCallum, K. 2006. Operating procedures. Prairie Recommending Committee for Wheat, Rye and Triticale. \[Online\] Available: http://www.pgdc.ca/pdfs/PRCWRT%20
                                
                                Updated%20Operating%20Procedures%202008.pdf.
                                
                                Gilbert, J., and Woods, S. 2006. Strategies and considerations for multi-location FHB screening nurseries. p. 93–102. In Ban, T., Lewis, J.M., and Phipps, E.E. (ed.) The global Fusarium initiative
                                
                                for international collaboration: A strategic planning workshop. CIMMYT, El Batàn, Mexico. 14–17 March 2006.CIMMYT, Mexico, D.F.
                                
                                Hagberg, S. 1960. A rapid method for determining alpha-amylase activity. Cereal Chem. 37: 218-222
                                
                                Menzies, J.G., Knox, R.E., Nielsen, J., and Thomas, P.L. 2003. Virulence of Canadian isolates of Ustilago tritici; 1964–1998, and the use of the geometric rule in understanding host differential
                                
                                complexity. Can. J. Plant Pathol. 25:62-72.doi:10.1080/07060660309507050
                                
                                Peterson, R.F., Campbell, A.B., and Hannah, A.E. 1948. A diagrammatic scale for estimating rust intensity on leaves and stems of cereals. Can J. Res. 26, Sec. C: 496–500.
                                
                                SAS Institute, Inc. 2003. SAS software. Version 9. SAS Institute Inc. Cary, NC
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


