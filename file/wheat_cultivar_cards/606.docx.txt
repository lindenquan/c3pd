**606**

  **Cultivar name**             606
  ----------------------------- -------------------------------------------------------------------------------------
  **Used names**                Granite
  **Origin**                    Germany/Canada
  **Year released**             2002
  **Year started**              1989
  **Affiliation of breeders**   Pflanzenzucht Oberlimpurg, Germany; C&M Seeds
  **Breeding method**           Pedigree
  **Type**                      Spring wheat, hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         Kent/B564//SO/3/Kent/2\*B564//Sap
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         Hege312-75-262/Chat"S”
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Kent/B564//SO/3/Kent/2\*B564//Sap/4/Hege312-75-262/Chat"S”
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00003138e.shtml


