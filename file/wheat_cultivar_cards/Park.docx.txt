**Park**

  **Cultivar name**             Park
  ----------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             1963
  **Year started**              1950
  **Affiliation of breeders**   Early Wheat Project Group, Lacombe Experimental Farm
  **Breeding method**           
  **Type**                      Spring wheat, hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         Mida-Cadet
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         Thatcher
  Origin:                       US
  Type:                         Spring wheat, hard red
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Mida-Cadet/Thatcher
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Similar to Saunders in maturity, has consistently outyielded Saunders, resistant to loose smut and some races of stem rust, susceptible to bunt and leaf rust
  **Reference**                 


