**AC Drummond**

  **Cultivar name**             AC Drummond
  ----------------------------- --------------
  **Used names**                Q.W. 534.14
  **Origin**                    Canada
  **Year released**             1997
  **Year started**              
  **Affiliation of breeders**   AAFC
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


