**Harus**

  **Cultivar name**             Harus
  ----------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                H 1-11-3
  **Origin**                    Canada
  **Year released**             1985
  **Year started**              1974
  **Affiliation of breeders**   Harrow Research Station (AAFC); SeCan Association
  **Breeding method**           Pedigree?
  **Type**                      Winter wheat, soft white (CEWW)
  **Pedigree**                  
  **Female parent**             
  Name:                         Fredrick
  Origin:                       Canada
  Type:                         Winter wheat, soft white (CEWW)
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Yorkstar
  Origin:                       US
  Type:                         Winter wheat
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Fredrick/Yorkstar
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               High-yielding, short-strawed, lodging-resistant, early-maturing, resistant to wheat spindle streak mosaic, yielded well in southwestern Ontario where corn heat units exceed 2700, intermediate in quality between Fredrick and Yorkstar, fairly resistant to sprouting, moderately susceptible to powdery mildew (Erysiphe graminis D.C. ex. Merat f. s.p. tritici Marchal), leaf rust (Puccinia recondite Rob. ex. Desm.), and speckled leaf disease (Septoria spp.), resistant to wheat spindle streak mosaic (Polymyxa graminis). Reaction to loose smut (Ustilago tritici (Pers.) Rostr.) and common bunt (Tilletia foetida) unknown
  **Reference**                 


