**Blackton**

  **Cultivar name**             Blackton
  ----------------------------- ----------
  **Used names**                
  **Origin**                    Canada
  **Year released**             1928
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Type**                      Durum
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


