**Harmil**

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Harmil
  ----------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                0-90-4-1

  **Origin**                    Canada

  **Year released**             1990

  **Year started**              1977

  **Affiliation of breeders**   Plant Research Centre, Research Branch (AAFC)

  **Breeding method**           Head-to-row selection

  **Type**                      Winter wheat, soft white

  **Pedigree**                  

  **Female parent**             

  Name:                         75-60-6

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Male parent**               

  Name:                         Fredrick

  Origin:                       Canada

  Type:                         Winter wheat, soft white (CEWW)

  Improvement status:           Cultivar

  Performance:                  

  **Pedigree**                  75-60-6/Fredrick

  **Pedigree description**      75-60-6, was an F3 plant from the cross 8077892-1/Tecumseh made in 1975 .
                                
                                8077B92-1 was bred by F. Gfeller from the complex cross:
                                
                                54117ab-2B5 (Cornell dwarf sel.)/6/Rio/Rex/2/Brevor/3/Brevor/Norin 10(Wash. No. 1)/4/Genesee/5/Capelle Desprez.
                                
                                The Cornell University dwarf 54117ab-2B5, from the program of Prof. N. F. Jensen, has the pedigree: Genesee/3/Yorkwin/2/Brevor/Norin 10

  **Pedigree tree**             

  **Performance**               Well adapted to southwestern Ontario, high yield, medium height, strong straw, low grain and flour protein, and low 1000-grain weight, moderately susceptible to leaf and head diseases, but it is the only cultivar available for the area that is resistant to the two prevalent races of loose smut (Ustilago tritici)

  **Reference**                 
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


