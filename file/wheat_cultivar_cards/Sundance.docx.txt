**Sundance**

  **Cultivar name**             Sundance
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                5520-8
  **Origin**                    Canada
  **Year released**             1971
  **Year started**              1953
  **Affiliation of breeders**   Lethbridge Research Station (AAFC)
  **Breeding method**           Bulk?
  **Type**                      Winter wheat, hard red (Canada Western Red Winter)
  **Pedigree**                  
  **Female parent**             
  Name:                         Cheyenne
  Origin:                       US
  Type:                         Winter wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Kharkov 22 MC
  Origin:                       Canada
  Type:                         Winter wheat, hard red
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Cheyenne/Kharkov 22 MC
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Adapted to the traditional winter wheat-growing areas of southern Alberta and southwestern Saskatchewan, superior to Winalta, the currently predominant winter wheat cultivar in Alberta in yielding potential and winterhardiness, the leaves show no tip browning and fraying, agronomically resembles Kharkov 22 MC but is more resistant to shattering, moderately resistant to common root rot, some resitance to common bunt, susceptible to leaf and stem rust
  **Reference**                 GRANT, M. N., PITTMAN, U. J., HORRICKS, J. S., HOlMES, N. D., ANDERSON, D. T. and SMITH, A. D. 1968. Winter wheat production in Western Canada. Can. Dep. Agr. Publ. 1056: 14 p


