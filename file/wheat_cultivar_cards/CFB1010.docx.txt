**CFB1010**

  **Cultivar name**             CFB1010
  ----------------------------- -------------------------------------------------------------------------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             2014 (applied, pending)
  **Year started**              
  **Affiliation of breeders**   Université Laval; La Coop Fédérée
  **Breeding method**           
  **Type**                      
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00009711e.shtml


