**CDC Chase**

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             CDC Chase
  ----------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                DH01-28-137

  **Origin**                    Canada

  **Year released**             2013

  **Year started**              2001

  **Affiliation of breeders**   Department of Plant Sciences, University of Saskatchewan

  **Breeding method**           Doubled haploid

  **Type**                      Winter wheat, hard red (Canada Western Red Winter)

  **Pedigree**                  

  **Female parent**             

  Name:                         McClintock

  Origin:                       Canada

  Type:                         Canada Western Red Winter

  Improvement status:           Cultivar

  Performance:                  

  **Male parent**               

  Name:                         S96-35

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Pedigree**                  McClintock/S96-35

  **Pedigree description**      McClintock=GN567/Norstar and S96-35=S86-808/Abilene

  **Pedigree tree**             

  **Performance**               High grain yield potential relative to the grain quality checks, CDC Buteo and Moats, is its primary strength. It has excellent stem, leaf, and stripe rust resistance, and low physiological leaf spot and very susceptible bunt ratings. Except for minor differences, its performance has been similar to that of CDC Buteo and Moats for the remainder of the agronomic characters measured. A suitable combination of grain quality, rust resistance, and grain yield make CDC Chase a good fit for the low to intermediate precipitation regions of western Canada

  **Reference**                 Fowler, D. B. 1999. CDC Falcon winter wheat. Can. J. Plant Sci. 79: 599601.
                                
                                Fowler, D. B. 2010. CDC Buteo hard red winter wheat. Can. J. Plant Sci. 90: 707710.
                                
                                Fowler, D. B. 2011. Accipiter hard red winter wheat. Can. J. Plant Sci. 91: 363365.
                                
                                Fowler, D. B. 2012. Moats hard red winter wheat. Can. J. Plant Sci. 92: 191193
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


