**White Russian**

  **Cultivar name**             White Russian
  ----------------------------- --------------------------------------------
  **Used names**                Surprise, Goldcoin, Wellman, Wellmans Fife
  **Origin**                    Canada
  **Year released**             1889
  **Year started**              
  **Affiliation of breeders**   Central Experimental Farm
  **Breeding method**           Selection
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Red Fife
  Origin:                       Canada
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  Red Fife
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


