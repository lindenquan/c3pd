**Kingsey**

  **Cultivar name**             Kingsey
  ----------------------------- --------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             2009
  **Year started**              
  **Affiliation of breeders**   SemiCan
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


