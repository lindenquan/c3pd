**AC Helena**

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             AC Helena
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                AW 356

  **Origin**                    Canada

  **Year released**             2000

  **Year started**              1992

  **Affiliation of breeders**   Crops and Livestock Research Centre (AAFC); W.G. Thompson and Sons Limited, Naim Research Laboratory

  **Breeding method**           Pedigree?

  **Type**                      Spring wheat, hard red

  **Pedigree**                  

  **Female parent**             

  Name:                         Ankra/Kolibri

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Male parent**               

  Name:                         QW534.40/3/Gamenya/Kolibri/2/Consens

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Pedigree**                  Ankra/Kolibri/4/QW534.40/3/Gamenya/Kolibri/2/Consens

  **Pedigree description**      

  **Pedigree tree**             

  **Performance**               Adapted to Ontario and the Maritimes, expressed high grain yield, lodging resistance, and a high level of resistance to powdery mildew, moderately resistant to shattering
                                
                                Moderately resistant to powdery mildew, with better resistance than Celtic and Grandin; slightly more susceptible to septoria leaf and glume blotch \[caused by Stagonospora nodorum (Berk.) Castellani and E.G. Germano\] than the checks; slightly more susceptible than Celtic but similar to Grandin to fusarium head blight (caused by Fusarium spp.); more susceptible to leaf and stem rust than the checks; and less susceptible to barley yellow dwarf virus than Grandin and similar to Celtic.

  **Reference**                 
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


