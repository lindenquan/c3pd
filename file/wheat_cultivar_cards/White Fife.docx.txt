**White Fife**

  **Cultivar name**             White Fife
  ----------------------------- ----------------------------------------------
  **Used names**                CItr 4412, REG1032, RL2, Ottawa 11, CAN 1560
  **Origin**                    Canada
  **Year released**             1879? Or 1908?
  **Year started**              
  **Affiliation of breeders**   Central Experimental Farm
  **Breeding method**           Selection
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Red Fife
  Origin:                       Canada
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  Red Fife
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


