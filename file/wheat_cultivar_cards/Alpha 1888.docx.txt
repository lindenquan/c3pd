**Alpha, 1888**

  **Cultivar name**             Alpha, 1888
  ----------------------------- ----------------------------
  **Used names**                HY-612, 87-W-153, HW Alpha
  **Origin**                    Canada
  **Year released**             1888
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Ladoga
  Origin:                       Russia
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         White Fife
  Origin:                       Canada
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Ladoga/White Fife
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


