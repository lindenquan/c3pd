**Arctic**

  **Cultivar name**             Arctic
  ----------------------------- -----------------------
  **Used names**                01SW9.13
  **Origin**                    Canada
  **Year released**             2007
  **Year started**              
  **Affiliation of breeders**   SemiCan Atlantic Inc.
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


