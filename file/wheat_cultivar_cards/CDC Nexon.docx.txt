**CDC Nexon **

  **Cultivar name**             CDC Nexon
  ----------------------------- ----------------------------
  **Used names**                SK0263, CDC Bavaria
  **Origin**                    Canada
  **Year released**             2002
  **Year started**              
  **Affiliation of breeders**   University of Saskatchewan
  **Breeding method**           
  **Type**                      Spring spelt
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


