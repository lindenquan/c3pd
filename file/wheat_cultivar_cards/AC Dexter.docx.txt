**AC Dexter**

  **Cultivar name**             AC Dexter
  ----------------------------- ---------------------------------------------------
  **Used names**                PRCW9201
  **Origin**                    Canada
  **Year released**             1996 (abandoned 1999)
  **Year started**              
  **Affiliation of breeders**   Eastern Cereal and Oilseed Research Centre (AAFC)
  **Breeding method**           
  **Type**                      Winter wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Houser
  Origin:                       US
  Type:                         Winter wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         MIB 5008
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Houser/MIB 5008
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


