**Fukuho DH**

  **Cultivar name**             Fukuho DH
  ----------------------------- --------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Fukuhokomugi
  Origin:                       Japan
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Maize
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  Fukuhokomugi/Maize
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


