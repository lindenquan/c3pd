**AC Avonlea**

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             AC Avonlea
  ----------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                DT661, 8774-CR34

  **Origin**                    Canada

  **Year released**             1997 (revoked 2012)

  **Year started**              1987

  **Affiliation of breeders**   SemiArid Prairie Agricultural Research Centre (AAFC)

  **Breeding method**           Pedigree

  **Type**                      Durum, amber

  **Pedigree**                  

  **Female parent**             

  Name:                         8267-AD2A

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Male parent**               

  Name:                         DT612

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Pedigree**                  8267-AD2A/DT612

  **Pedigree description**      The parent 8267-AD2A derives from the cross DT379/DT367, and DT612 derives from the cross DT367/Medora. DT367 is S017/Wascana//
                                
                                7168, and DT379 is D6676/Quilafen

  **Pedigree tree**             

  **Performance**               High yield and high grain protein concentration. It has shorter, stronger straw than Kyle and Plenty, and has similar maturity and disease resistance

  **Reference**                 <http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00001446e.shtml>
                                
                                Hoffmann, J. A. and Metzger, R. J. 1976. Current status of virulence genes and pathogenic races of the wheat bunt fungi in the northwestern USA. Phytopathology 66: 657–660.
                                
                                Kolmer, J. A. 1994. Physiologic specialization of Puccinia recondita f.sp. tritici in Canada in 1993. Can. J. Plant Pathol. 16:
                                
                                326–328.
                                
                                Long, D. L. and Kolmer, J. A. 1989. A North American system of nomenclature for Puccinia recondita f.sp. tritici.
                                
                                Phytopathology 79: 525–529.
                                
                                Nielsen, J. 1987. Races of Ustilago tritici and techniques for their study. Can. J. Plant Pathol. 9: 91–105.
                                
                                Roelfs, A. P. and Martens, J. W. 1988. An international system of nomenclature for Puccinia graminis f. sp. tritici. Phytopathology 78: 525–533
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


