**Florence**

  **Cultivar name**             Florence
  ----------------------------- --------------------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             1892
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Alpha, 1888
  Origin:                       Canada
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Hard Red Calcutta
  Origin:                       India
  Type:                         Spring wheat
  Improvement status:           
  Performance:                  
  **Pedigree**                  Alpha, 1888/ Hard Red Calcutta
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


