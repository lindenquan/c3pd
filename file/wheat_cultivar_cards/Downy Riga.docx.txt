**Downy Riga**

  **Cultivar name**             Downy Riga
  ----------------------------- --------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             1891
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Onega
  Origin:                       Russia
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Gehun
  Origin:                       India
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Onega/Gehun
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


