**Hallmark**

  **Cultivar name**             Hallmark
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                ACS98735
  **Origin**                    Germany
  **Year released**             2007
  **Year started**              1991
  **Affiliation of breeders**   Planzenzucht Oberlimpurg, Germany; C&M Seeds
  **Breeding method**           Bulk
  **Type**                      Spring type, amber
  **Pedigree**                  
  **Female parent**             
  Name:                         FR 90/005
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         FR 91/018
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  FR 90/005//FR 91/018
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Susceptible to Septoria tritici blotch (Septoria tritici), moderately resistant to Powdery mildew (Erysiphe graminis, f.sp. tritici), resistant to Leaf rust (Puccinia triticina)
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00005431e.shtml


