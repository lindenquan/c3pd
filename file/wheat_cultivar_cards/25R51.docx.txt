**25R51**

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             25R51
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                

  **Origin**                    US

  **Year released**             2007

  **Year started**              1995

  **Affiliation of breeders**   Pioneer Hi-Bred Ltd

  **Breeding method**           Pedigree

  **Type**                      Winter wheat, soft red

  **Pedigree**                  

  **Female parent**             

  Name:                         WBI1097S1/25R18 sib.

  Origin:                       

  Use type:                     

  Improvement status:           Breeding line

  Performance:                  

  **Male parent**               

  Name:                         2571

  Origin:                       

  Use type:                     

  Improvement status:           Breeding line

  Performance:                  

  **Pedigree**                  WBI1097S1/25R18 sib.//2571

  **Pedigree description**      WBI1097S1 is an experimental line derived from the cross WBZ053G//2555/Hart//2550. WBZ053G is an experimental line derived from the cross 'Kavkaz'/'Hart'//2550. The detailed parentage of the new variety is Kavkaz/Hart//2550/3/2555 sib./2548/4/25R18 sib/5/2571
                                
                                NOTE: cannot make sense of pedigree

  **Pedigree tree**             

  **Performance**               slightly resistant to Septoria tritici blotch (*Septoria tritici*), moderately susceptible to powdery mildew (*Erysiphe graminis, f.sp. tritici*), resistant to moderately resistant to Fusarium head blight (*Fusarium graminearum*), moderately resistant to Leaf rust (*Puccinia triticina*) and Stripe rust (*Puccinia striiformis*), susceptible to Spindle Streak Mosaic Virus, moderately susceptible to susceptible to Soil Borne Mosaic Virus, fair winter survival, moderate pre-harvest sprouting tendency

  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00006335e.shtml
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


