**BW87**

  **Cultivar name**             BW87
  ----------------------------- -------------------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             
  **Year started**              
  **Affiliation of breeders**   Cereal Research Centre (AAFC)
  **Breeding method**           
  **Type**                      Spring wheat, hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         RL4302//RL4356
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         BW4359/RL4353
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  RL4302/RL4356//BW4359/RL4353
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


