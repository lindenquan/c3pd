**Napayo**

  **Cultivar name**             Napayo
  ----------------------------- -----------------------------------------------------------------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             1972
  **Year started**              1952
  **Affiliation of breeders**   Winnipeg Research Station (AAFC)
  **Breeding method**           Backcross
  **Type**                      Spring wheat, hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         Manitou\*2
  Origin:                       Canada
  Type:                         Spring wheat, hard red
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         RL 4124.1
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Manitou\*2/RL 4124.1
  **Pedigree description**      RL 4124.1= Thatcher\*5/Lee/3/Thatcher\*7/Frontana//Thatcher\*6/Kenya Garmer
  **Pedigree tree**             
  **Performance**               Disease resistance is similar to Manitou
  **Reference**                 


