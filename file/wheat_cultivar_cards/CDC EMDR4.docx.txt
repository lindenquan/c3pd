**CDC EMDR-4**

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             CDC EMDR-4
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                

  **Origin**                    Canada

  **Year released**             2001

  **Year started**              \~1990

  **Affiliation of breeders**   Crop Development Centre and Department of Plant Sciences, University of Saskatchewan

  **Breeding method**           Pedigree?

  **Type**                      Spring wheat, red

  **Pedigree**                  

  **Female parent**             

  Name:                         AUS1293

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Male parent**               

  Name:                         Park

  Origin:                       Canada

  Type:                         Spring wheat, hard red (CWRS)

  Improvement status:           Cultivar

  Performance:                  

  **Pedigree**                  AUS1293/Park

  **Pedigree description**      

  **Pedigree tree**             

  **Performance**               Early maturing germplasm line, high levels of seed dormancy, agronomic performance is comparable to that of Columbus except had a soft endosperm texture

  **Reference**                 Hucl, P. 1995. Divergent selection for sprouting resistance in spring wheat. Plant Breed. 114: 199–204.
                                
                                Mares, D. J. 1987. Pre-harvest sprouting tolerance in white-grained wheat. Page 64 in D. J. Mares, ed. Proc 4th Int. Symp. Pre-harvest sprouting in cereals, Westview Press, Boulder, CO.
                                
                                Matus-Cádiz, M., Hucl, P. and Munasinghe, G. 2001. Seed dormancy and germination in three annual canarygrass (Phalaris canariensis) cultivars relative to spring wheat (Triticum aestivum). Seed Sci. Technol. 29: 523–531.
                                
                                McCaig, T. N. and DePauw, R. M. 1992. Breeding for pre-harvest sprouting tolerance in white-seed-coat spring wheat. Crop Sci.
                                
                                32: 19–25.
                                
                                Noll, J. S., Dyck, P. L. and Czarnecki, E. 1982. Expression of
                                
                                RL4137 type of dormancy in F1 seeds of reciprocal crosses in common wheat. Can. J. Plant Sci. 62: 345–349.
                                
                                Zadoks, J. C., Chang, T. T. and Konzak, C. F. 1974. A decimal code for the growth stages of cereals. Weed Res. 14: 415–421
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


