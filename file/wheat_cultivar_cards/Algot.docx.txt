**Algot**

  **Cultivar name**             Algot
  ----------------------------- ----------------------------------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             1993
  **Year started**              
  **Affiliation of breeders**   Thompsons Ltd.; Dow AgroSciences Canada Inc.
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


