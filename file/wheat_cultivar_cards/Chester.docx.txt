**Chester**

  **Cultivar name**             Chester
  ----------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                7743-151, BW 500, CT 786
  **Origin**                    Canada
  **Year released**             1976
  **Year started**              1960
  **Affiliation of breeders**   Lethbridge Research Station (AAFC)
  **Breeding method**           
  **Type**                      Spring wheat, hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         Renown/S-165/Rescue/Kendee
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         Mida/Cadet
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Renown/S-165/Rescue/Kendee/(Mida/Cadet)
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Mid-season maturity, resistant to shattering, moderately resistant to lodging, resistant to sawfly, resistant to common root rot (Cochliobolus sativus (Ito and Kurib.) Drechsl. Ex Dastur) and bunt (Tilletia foetida (Wallr.) Liro and Tilletia caries (DC.) Tul.), moderately resistant to stem rust (Puccinia graminis Pers. f. sp. Tritici Eriks. and E. Henn.) and susceptible to leaf rust (Puccinia recondite Rob. Ex. Desm. F. sp. Tritici) and loose smut (Ustilago tritici (Pers.) Rostr.)
  **Reference**                 


