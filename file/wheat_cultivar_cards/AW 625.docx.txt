**AW 625**

  **Cultivar name**             AW 625
  ----------------------------- -----------------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             2011
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         AC Helena/Quantum
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         AC Walton
  Origin:                       Canada
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  AC Helena/Quantum/AC Walton
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


