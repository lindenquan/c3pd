**Hard White Alpha**

  **Cultivar name**             Hard White Alpha
  ----------------------------- --------------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Type**                      Spring wheat, white
  **Pedigree**                  
  **Female parent**             
  Name:                         AC Foremost
  Origin:                       Canada
  Type:                         Spring wheat, red (CPSR)
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Alpha 16
  Origin:                       Canada
  Type:                         Spring wheat
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  AC Foremost/Alpha 16
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


