**CDC Thrive**

  **Cultivar name**             CDC Thrive
  ----------------------------- -------------------------------------------------------------------------------------
  **Used names**                PT575
  **Origin**                    Canada
  **Year released**             2012
  **Year started**              1999
  **Affiliation of breeders**   University of Saskatchewan; Crop Development Centre; Cargill Limited
  **Breeding method**           Pedigree? Bulk?
  **Type**                      Spring wheat, common hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         CDC Bounty
  Origin:                       Canada
  Type:                         Spring wheat, red (CWRS)
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         CDC Imagine
  Origin:                       Canada
  Type:                         Spring wheat, red (CWRS)
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  CDC Bounty/CDC Imagine
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Resistant to imidazolinone herbicide
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00007707e.shtml


