**Kanadskaya, K-10894**

  **Cultivar name**             Kanadskaya, K-10894
  ----------------------------- ---------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             1924
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Type**                      Winter wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


