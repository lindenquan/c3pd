**Ben**

  **Cultivar name**             Ben
  ----------------------------- -----------------------------------------------------------------------------------------------------------
  **Used names**                
  **Origin**                    US
  **Year released**             1998 (applied, withdrawn 2002)
  **Year started**              
  **Affiliation of breeders**   NDSU Research Foundation, US; North Dakota Agricultural Experiment Station; Performance Seeds Canada Ltd.
  **Breeding method**           
  **Type**                      
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00001434e.shtml


