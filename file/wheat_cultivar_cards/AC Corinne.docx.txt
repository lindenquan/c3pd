**AC Corinne**

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             AC Corinne
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                92W537, 84TRR125

  **Origin**                    Canada

  **Year released**             1999

  **Year started**              1976

  **Affiliation of breeders**   Cereal Research Centre (AAFC)

  **Breeding method**           Backcross and bulk

  **Type**                      Spring wheat, hard red (Canada Western Extra Strong)

  **Pedigree**                  

  **Female parent**             

  Name:                         Glenlea

  Origin:                       Canada

  Type:                         Spring wheat

  Improvement status:           Cultivar

  Performance:                  

  **Male parent**               

  Name:                         RL 4137

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Pedigree**                  Glenlea\*6/RL4137

  **Pedigree description**      RL 4137, a preharvest sprouting resistant line, was derived from the cross: Frontana/4/McMurachy//Exchange/3/2\*Redman/5/Thatcher\*6/Kenya Farmer

  **Pedigree tree**             

  **Performance**               Superior preharvest sprouting resistance and improved leaf rust resistance compared to Glenlea and Wildcat, and is higher yielding than Wildcat. AC Corinne has extra strong wheat quality similar to Glenlea combined with higher grain protein content, adapted to the wheat growing areas of the prairie provinces

  **Reference**                 Hoffmann, J. A. and Metzger, R. J. 1976. Current status of virulence genes and pathogenic races of the wheat bunt fungi in the
                                
                                northwestern USA. Phytopathology 66: 657–660.
                                
                                Kolmer, J. A. 1994. Physiologic specialization of Puccinia recondita f.sp. tritici in Canada in 1993. Can. J. Plant Pathol. 16:
                                
                                326–328.
                                
                                Long, D. L. and Kolmer, J. A. 1989. A North American system of nomenclature for Puccinia recondita f.sp.tritici.. Phytopathology 79: 525–529.
                                
                                Nielsen, J. 1987. Races of Ustilago tritici and techniques for their
                                
                                study. Can. J. Plant Pathol. 9: 91–105.
                                
                                Roelfs, A. P. and Martens, J. W. 1988. An international system of nomenclature for Puccinia graminis f. sp. tritici. Phytopathology 78: 525–533
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


