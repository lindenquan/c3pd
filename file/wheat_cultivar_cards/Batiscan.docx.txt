**Batiscan**

  **Cultivar name**             Batiscan
  ----------------------------- -----------------------
  **Used names**                01SW2.33, V1035
  **Origin**                    Canada
  **Year released**             2006
  **Year started**              
  **Affiliation of breeders**   SemiCan Atlantic Inc.
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


