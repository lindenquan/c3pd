**CDC Utmost**

  **Cultivar name**             CDC Utmost
  ----------------------------- -------------------------------------------------------------------------------------
  **Used names**                BW883
  **Origin**                    Canada
  **Year released**             2012
  **Year started**              1998
  **Affiliation of breeders**   Crop Development Centre; University of Saskatchewan; FP Genetics Inc.
  **Breeding method**           Pedigree? Bulk?
  **Type**                      Spring wheat, common hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         AC Elsa
  Origin:                       Canada
  Type:                         Spring wheat, hard red (CWRS)
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         CDC Teal/Seneca DH\#10
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Elsa//CDC Teal/Seneca DH\#10
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Moderately resistant to orange wheat blossom midge (Sitodiplosis mosellana)
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00007656e.shtml


