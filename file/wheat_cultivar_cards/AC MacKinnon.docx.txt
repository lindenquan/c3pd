**AC MacKinnon**

  **Cultivar name**             AC MacKinnon
  ----------------------------- ---------------------------------------------------
  **Used names**                PRSC9308 (82-28-32-1)
  **Origin**                    Canada
  **Year released**             1997 (applied, withdrawn 2000)
  **Year started**              
  **Affiliation of breeders**   Eastern Cereal and Oilseed Research Centre (AAFC)
  **Breeding method**           
  **Type**                      Winter wheat, white (CEWW)
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


