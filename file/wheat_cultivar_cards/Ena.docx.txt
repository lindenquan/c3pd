**Ena**

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Ena
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                H60-2

  **Origin**                    Canada

  **Year released**             1988

  **Year started**              1975

  **Affiliation of breeders**   Harrow Research Station (AAFC)

  **Breeding method**           Single seed descent

  **Type**                      Winter wheat, soft white

  **Pedigree**                  

  **Female parent**             

  Name:                         Frederick

  Origin:                       Canada

  Type:                         Winter wheat, soft white (CEWW)

  Improvement status:           Cultivar

  Performance:                  

  **Male parent**               

  Name:                         Houser

  Origin:                       US

  Type:                         Winter wheat

  Improvement status:           Cultivar

  Performance:                  

  **Pedigree**                  Frederick/Houser

  **Pedigree description**      

  **Pedigree tree**             

  **Performance**               Less susceptibility to scab caused by Fusarium graminearum Schwabe, and when infected, accumulates less of the associated mycotoxin, DON (deoxynivalenol), than other cultivars recommended for Ontario. In its area of adaptation, area I of southwestern Ontario where corn heat units are 2900 or more, it yields more than any of the recommended cultivars, except Augusta, which yields
                                
                                l% more.
                                
                                Less susceptible to leaf rust than all check cultivars, moderately susceptible to powdery mildew, reaction to loose smut (Ustilago tritici (Pers.) Rostr.) and common bunt (Tilletia foetida (Wall.) Liro and Tilletia caies (DC.) Tul.) unknown, more susceptible to sprouting then Harus, and less susceptible to sprouting than Houser

  **Reference**                 
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


