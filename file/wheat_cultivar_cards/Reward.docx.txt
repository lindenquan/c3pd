**Reward**

  **Cultivar name**             Reward
  ----------------------------- -------------------------------------------------------------------
  **Used names**                Ottawa 928, Citr 8182, TD-7, AUS 7397
  **Origin**                    Canada
  **Year released**             1927
  **Year started**              
  **Affiliation of breeders**   Canada Department of Agricultre; Winnipeg Research Station (AAFC)
  **Breeding method**           
  **Type**                      Spring wheat, hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         Marquis
  Origin:                       Canada
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Prelude
  Origin:                       Canada
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  Marquis/Prelude
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


