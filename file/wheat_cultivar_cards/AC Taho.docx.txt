**AC Taho**

  **Cultivar name**             AC Taho
  ----------------------------- --------------
  **Used names**                CM-RL4719
  **Origin**                    Canada
  **Year released**             1998
  **Year started**              
  **Affiliation of breeders**   C&M Seeds
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


