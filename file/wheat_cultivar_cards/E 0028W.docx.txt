**E 0028W**

  **Cultivar name**             E 0028W
  ----------------------------- ----------------------------
  **Used names**                E 0028
  **Origin**                    Canada
  **Year released**             2008
  **Year started**              
  **Affiliation of breeders**   Genesis Brand Seed Limited
  **Breeding method**           
  **Type**                      Winter wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


