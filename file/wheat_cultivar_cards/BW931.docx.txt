**BW931**

  **Cultivar name**             BW931
  ----------------------------- ------------------------------------------------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             2012
  **Year started**              
  **Affiliation of breeders**   SemiArid Prairie Agricultural Research Centre (AAFC)
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         AC Superb/CDC Osler
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         ND 744
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  AC Superb/CDC Osler
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


