**Gryphon**

  **Cultivar name**             Gryphon
  ----------------------------- -------------------------------------------------------------------------------------
  **Used names**                5371, C.M.-96097
  **Origin**                    Germany
  **Year released**             2001 (applied, withdrawn 2004)
  **Year started**              
  **Affiliation of breeders**   Pflanzenzucht Oberlimpurg, Germany; C&M Seeds
  **Breeding method**           
  **Type**                      Winter wheat, soft white
  **Pedigree**                  
  **Female parent**             
  Name:                         CD 7561/Genesee/2/CD 7561/Kent/3/7453-4-2-4
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         Yorkstar
  Origin:                       US
  Type:                         Winter wheat
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  CD 7561/Genesee/2/CD 7561/Kent/3/7453-4-2-4/4/Yorkstar
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00003506e.shtml


