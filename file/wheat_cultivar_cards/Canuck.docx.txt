**Canuck**

  **Cultivar name**             Canuck
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                C.T. 774
  **Origin**                    Canada
  **Year released**             1974
  **Year started**              1957
  **Affiliation of breeders**   Swift Current Research Station (AAFC)
  **Breeding method**           
  **Type**                      Spring wheat, hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         Canthatch
  Origin:                       Canada
  Type:                         Spring wheat, hard red
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Mida/Cadet/Rescue
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Cathatch/(Mida/Cadet/Rescue)
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Resistant to common root rot (Biploaris sorokiniana \[Sacc. In Sorok.\] Shoem. And Fusarium sp.), loose smut (Ustilago tritici \[Pers.\] Rostr.) and head discoloration. Susceptible to leaf rust (Puccinia recondite Rob. Ex. Desm. F. sp. Tritici) and stem rust (Puccinia graminis Pers. f. sp. Tritici Eriks. and E. Henn.), and to bunt (Tilletia foetida \[Wallr.\] Liro), moderately susceptible to shattering, moderately susceptible to lodging, resistant to sawfly, medium late maturity
  **Reference**                 


