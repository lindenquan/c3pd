**Kinley**

  **Cultivar name**             Kinley
  ----------------------------- --------------
  **Used names**                
  **Origin**                    Canada
  **Year released**             1923
  **Year started**              
  **Affiliation of breeders**   
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


