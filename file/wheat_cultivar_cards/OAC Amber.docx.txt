**OAC Amber**

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             OAC Amber
  ----------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                VA05WD-31

  **Origin**                    US

  **Year released**             2010

  **Year started**              1998

  **Affiliation of breeders**   University of Guelph; Virginia Polytechnic Institute and State University; Business Development Office; C&M Seeds

  **Breeding method**           Bulk

  **Type**                      Winter durum, amber

  **Pedigree**                  

  **Female parent**             

  Name:                         N736-89/Urraca

  Origin:                       

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Male parent**               

  Name:                         N1439-83

  Origin:                       Ukraine

  Type:                         

  Improvement status:           Breeding line

  Performance:                  

  **Pedigree**                  N736-89/Urraca//N1439-83

  **Pedigree description**      Parentage of N736-89 is ‘Chernomor’/N367-78(‘Kharkovskaya I’/‘Odesskaya Yubilejnaya’)//‘Oviachic65’/‘Kharkovskaya I. Parentage of N1439-83 is Kharkovskaya I/‘Odesskaya Yubilejnaya’//Oviachic
                                
                                65/‘Zernogradskaya 2087-76’. ‘Urraca’ was developed by the Turkish National Wheat Improvement Program, and derived from the cross ‘Uveyik 162’/ND61.130//‘Akbasak 073-44’/‘Oviachic 65’/3/‘Berkmen’/Oviachic65/4/C.BUG1018//BR180/‘Wells’

  **Pedigree tree**             

  **Performance**               Awned wheat with amber colored kernels, high test weight, kernel weight, and protein level with good winter hardiness. OAC Amber has good resistance to leaf rust (Puccinia triticina) but is moderately susceptible to powdery mildew (Blumeria graminis) and leaf blotch (Septoria tritici), and susceptible to Fusarium head blight (FHB). OAC Amber is well adapted for the winter wheat growing areas of Ontario

  **Reference**                 
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


