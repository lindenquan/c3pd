**HY162-HRF**

  **Cultivar name**             HY162-HRF
  ----------------------------- ----------------------------------------------------------------------------------------------------------------
  **Used names**                
  **Origin**                    Canada?
  **Year released**             2011 (applied, abandoned 2013)
  **Year started**              
  **Affiliation of breeders**   Agrigeneticcs, Inc. (A Division of Dow AgroSciences Inc.), US; Hyland Seeds (A Division of AgroSciences, Inc.)
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Male parent**               
  Name:                         
  Origin:                       
  Type:                         
  Improvement status:           
  Performance:                  
  **Pedigree**                  
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00008304e.shtml


