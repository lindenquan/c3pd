**Harvard**

  **Cultivar name**             Harvard
  ----------------------------- -------------------------------------------------------------------------------------
  **Used names**                ACS 98036
  **Origin**                    Germany
  **Year released**             2003
  **Year started**              1987
  **Affiliation of breeders**   Pflanzenzucht Oberlimpurg, Germany; C&M Seeds
  **Breeding method**           Pedigree
  **Type**                      Winter wheat, hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         PF 626/80
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         FHB 8801/78
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  PF 626/80//FHB 8801/78
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00003660e.shtml


