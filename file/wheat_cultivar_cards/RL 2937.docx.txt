**RL 2937**

  **Cultivar name**             RL 2937
  ----------------------------- ---------------------
  **Used names**                CT 321
  **Origin**                    Canada
  **Year released**             1955
  **Year started**              
  **Affiliation of breeders**   Rust laboratory
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Lee\*6
  Origin:                       US
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Kenya Farmer
  Origin:                       Kenya
  Type:                         Spring wheat
  Improvement status:           Cutlivar
  Performance:                  
  **Pedigree**                  Lee\*6/Kenya Farmer
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


