**Messier**

  **Cultivar name**             Messier
  ----------------------------- -------------------
  **Used names**                SH78-210
  **Origin**                    Canada
  **Year released**             1986
  **Year started**              
  **Affiliation of breeders**   SeCan Association
  **Breeding method**           
  **Type**                      Spring wheat
  **Pedigree**                  
  **Female parent**             
  Name:                         Opal
  Origin:                       Germany
  Type:                         Spring wheat
  Improvement status:           Cultivar
  Performance:                  
  **Male parent**               
  Name:                         Nadadores-63
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Opal/Nadadores-63
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               
  **Reference**                 


