**Brigade**

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Cultivar name**             Brigade
  ----------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                DT773, A9909-HD3D

  **Origin**                    Canada

  **Year released**             2008

  **Year started**              1999

  **Affiliation of breeders**   SemiArid Prairie Agricultural Research Centre (AAFC); Office of Intellectual Property and Commercialization (AAFC)

  **Breeding method**           Pedigree

  **Type**                      Durum, amber

  **Pedigree**                  

  **Female parent**             

  Name:                         DT513

  Origin:                       Canada

  Type:                         Durum

  Improvement status:           Breeding line

  Performance:                  

  **Male parent**               

  Name:                         DT696

  Origin:                       Canada

  Type:                         Durum

  Improvement status:           Breeding line

  Performance:                  

  **Pedigree**                  DT513/DT696

  **Pedigree description**      DT513= DT625/DT612
                                
                                DT696= DT618/DT636//Kyle

  **Pedigree tree**             

  **Performance**               Resistance to lead rust (Puccinia triticina) and stem rust (Puccinia graminis f. sp. tririci), moderately resistant to moderately susceptible to Tan spot (Pyrenophora triticirepentis), moderately resistant to moderately susceptible to Septoria tritici blotch (Septoria tritici), moderately resistant to moderately susceptible to Fusarium head blight (Fusarium graminearum, Fusarium species), resistant to Common bunt (Tilleria caries, Tilletia foetida), adapted to the durum production area of Canadian prairies, combines yield similar to the checks, very strong gluten, and low grain cadmium concentration, better straw strength than Strongfield, slightly later maturity

  **Reference**                 <http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00007319e.shtml>
                                
                                Clarke, J. M., Leisle, D. and Kopytko, G. L. 1997. Inheritance of cadmium concentration in five durum wheat crosses. Crop Sci. 37: 17221726.
                                
                                Clarke, J. M., McCaig, T. N., DePauw, R. M., Knox, R. E., Clarke, F. R., Fernandez, M. R. and Ames, N. P. 2005. Strongfield durum wheat. Can. J. Plant Sci. 85: 651654
                                
                                Fetch, T. G. 2005. Races of Puccinia graminis on wheat, barley,
                                
                                and oat in Canada, in 2002 and 2003. Can. J. Plant Pathol. 27:
                                
                                572580.
                                
                                Hoffmann, J. A. and Metzger, R. J. 1976. Current status of virulence genes and pathogenic races of the wheat bunt fungi in the northwestern USA. Phytopathology 66: 657660.
                                
                                Long, D. L. and Kolmer, J. A. 1989. A North American system of nomenclature for Puccinia recondita f.sp. tritici. Phytopathology 79:525529.
                                
                                McCallum, B. D. and Seto-Goh, P. 2006. Physiologic specialization of Puccinia triticina, the causal agent of wheat leaf, in Canada in 2004. Can. J. Plant Pathol. 28: 566576.
                                
                                McFadden, W., 1991. Etiology and epidemiology of leafspotting diseases in winter wheat in Saskatchewan. Ph.D. thesis, University of Saskatchewan, Saskatoon, SK. 151 pp.
                                
                                Nielsen, J. 1987. Races of Ustilago tritici and techniques for their study. Can. J. Plant Pathol. 9:91105.
                                
                                Penner, G. A., Clarke, J., Bezte, L. J. and Leisle, D. 1995. Identification of RAPD markers linked to a gene governing cadmium uptake in durum wheat. Genome 38: 543547.
                                
                                Roelfs, A. P. and Martens, J. W. 1988. An international system of nomenclature for Puccinia graminis f. sp. tritici. Phytopathology 78:525533
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


