**Keldin**

  **Cultivar name**             Keldin
  ----------------------------- -------------------------------------------------------------------------------------
  **Used names**                ACS 55017
  **Origin**                    Germany
  **Year released**             2011
  **Year started**              1996
  **Affiliation of breeders**   Pflanzenzucht Oberlimpurg, Germany; C&M Seeds
  **Breeding method**           Pedigree
  **Type**                      Winter wheat, common hard red
  **Pedigree**                  
  **Female parent**             
  Name:                         Bernburg 235/Carlisle
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         TRX-A16-3-2
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Pedigree**                  Bernburg 235/Carlisle//TRX-A16-3-2
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Good resistance to shattering, fair to good winter survival
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00007817e.shtml


