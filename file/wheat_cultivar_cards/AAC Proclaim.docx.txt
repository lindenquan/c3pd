**AAC Proclaim**

  **Cultivar name**             AAC Proclaim
  ----------------------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Used names**                GP80, SWS416
  **Origin**                    Canada
  **Year released**             2015
  **Year started**              2004
  **Affiliation of breeders**   Agriculture & Agri-Food Canada – Office of Intellectual Property and Commercialization
  **Breeding method**           Bulk breeding technique
  **Type**                      Spring wheat, soft red
  **Pedigree**                  
  **Female parent**             
  Name:                         FHB37
  Origin:                       
  Type:                         
  Improvement status:           Breeding line
  Performance:                  
  **Male parent**               
  Name:                         AC Reed
  Origin:                       Canada
  Type:                         Spring, soft white
  Improvement status:           Cultivar
  Performance:                  
  **Pedigree**                  FHB37/AC Reed
  **Pedigree description**      
  **Pedigree tree**             
  **Performance**               Resistant to Fusarium Head Blight (Fusarium graminearum, Fusarium species) and Leaf Rust (Puccinia triticina), moderately resistant to Loose Smut (Ustilago tritici) and Stem Rust (Puccinia graminis f. sp. tritici), moderately susceptible to Kernel Black Point (principally caused by Alternaria alternata) and susceptible to Stripe Rust (Puccinia striiformis), good resistance to shattering
  **Reference**                 http://www.inspection.gc.ca/english/plaveg/pbrpov/cropreport/whe/app00009021e.shtml


