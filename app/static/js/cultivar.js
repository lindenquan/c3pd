$(main);

function init() {
  $('body').scrollspy({ target: '#scrollspy' });
  $('.cultivar-link').click(function() {
    var name = replaceSlash($.trim($(this).text()));
    var href = variables.currentURL + 'cultivar/' + variables.cropName + '/' + name;
    var href = variables.currentURL + 'cultivar/' + variables.cropName + '/' + name;
    window.open(href,'_blank');

  });
  $('g text tspan').click(function() {
    var name = replaceSlash($.trim($(this).text().replace(/\(\d+\)/, '')));
    if (name[0] == '(')
      return
    var href = variables.currentURL + 'cultivar/' + variables.cropName + '/' + name;
    window.open(href,'_blank');

  });
  $('g text tspan').on("mouseover", function() {
    var name = replaceSlash($.trim($(this).html()));
    if (name[0] == '(')
      return;
    //var style = $(this).attr('style').split(';');
    //style[0] = 'fill:#34BC9D'
    //$(this).attr('style', style.join(';'));
    $(this).attr('fill', '#34BC9D');
    $(this).css('cursor', 'pointer');
  });

  $('g text tspan').on("mouseout", function() {
    //var style = $(this).attr('style').split(';');
    //style[0] = 'fill:#000'
    //$(this).attr('style', style.join(';'));
    $(this).attr('fill', '#000');
  });

  if ($('#pedigree-image').length > 0){
    $('.tree').treemenu({ delay: 150, expand:false });
  } else {
    $('.tree').treemenu({ delay: 150, expand:true });
  }
  
  $('#expand-tree').click(function() { $('.tree').expandTree(); });
  $('#collapse-tree').click(function() { $('.tree').collapseTree(); });

}

function main() {
  init();
}
