function initTable(columns, data) {
  $('.loader').css("display", "none");
  variables.mainTable = $('#main-table').DataTable({
    columns: columns,
    data: data,
    scrollX: true,
    pagingType: "input",
    aaSorting: [
      [1, 'dsc'], [0, 'asc']
    ],
    pageLength: 15,
    aLengthMenu: [15, 25, 50, 100],
    createdRow: function(row, data, index) {
       $('td', row).eq(0).html('<a target="_blank" href="'+variables.currentURL+'cultivar/'+variables.crop+'/'+ replaceSlash(data[0]) +'">'+data[0]+'</a>')
    },
    initComplete: function(settings, json) {
      if (variables.keyword != 'None') {
        $('#main-table').DataTable().search(variables.keyword, true, true).draw();
      }
    }
  });
}

function initTableSection() {
  $('#main-table-section').empty();
  $('#main-table-section').append('<div class="loader"></div><table id="main-table" class="display" width="100%" cellspacing="0"></table>');
}

function loadTableWithLocalData(columns, data) {
  initTableSection();
  setTimeout(function() { initTable(columns, data) }, 100);
}

function loadTable() {
  initTableSection();
  $.ajax({
    url: variables.currentURL + "main_table/" + variables.crop,
    type: "POST",
    dataType: "json",
    success: function(result) {
      var length = result.columns.length;

      for (i = 0; i < length; i++) {
        result.columns[i].title = result.columns[i].title.replace('_', ' ').capitalize();
      }
      variables.cropDataCache[variables.crop] = {};
      variables.cropDataCache[variables.crop].columns = result.columns;
      variables.cropDataCache[variables.crop].data = result.data;
      initTable(result.columns, result.data);
    }
  });
}

function initFilter() {
  $('#cultivar-filter [type=checkbox]').change(
    function(event) {
      if (variables.mainTable != undefined) {
        var allNull = true;
        $('[type=checkbox]', $(this).parents('.panel-body')).each(function() {
          if (this.checked) {
            allNull = false;
            return;
          }
        });
        if (allNull) {
          $(this).parents(".panel-body").attr('all-null', '');
        } else {
          $(this).parents(".panel-body").removeAttr('all-null');
        }
        variables.mainTable.draw();
      } else {
        this.checked = false;
      }
    });
  $('#cultivar-filter button').click(function(event) {
    event.preventDefault();
    if (variables.mainTable != undefined) {
      variables.mainTable.draw();
    }
  });
  $('.panel-collapse').first().addClass('in');
}


function customTableFilter() {
  $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
      var show = true;
      $('#cultivar-filter .panel-body').each(function(index) {
        index++;
        var valInTable = data[index];
        if ($(this).is('[all-null]')) {
          return true;
        } else {
          var checkboxes = $('[type=checkbox]', this);
          if (checkboxes.length) {
            var foundMatch = false;
            checkboxes.each(function() {
              if (this.checked) {
                if (valInTable === $(this).val()) {
                  foundMatch = true;
                  return false;
                }
              }
            });
            if (foundMatch) {
              return true;
            } else {
              show = false;
              return false;
            }

          }
          var fromTo = $('[type=text]', this);
          if (fromTo.length) {
            var min = parseInt(fromTo.eq(0).val(), 10);
            var max = parseInt(fromTo.eq(1).val(), 10);
            var numInTable = parseInt(valInTable, 10);
            if ((isNaN(min) && isNaN(max)) ||
              (isNaN(min) && numInTable <= max) ||
              (min <= numInTable && isNaN(max)) ||
              (min <= numInTable && numInTable <= max)) {
              return true;
            } else {
              show = false;
              return false;
            }
          }
        }
      });
      return show;
    }
  );
}

function cropChange() {
  $('#crop-selection').on('change', function(e) {
    if (variables.crop == this.value) {
      return;
    } else {
      variables.crop = this.value;
      document.title = variables.crop + ': cultivar list'
      loadFilterHTML();
      var cropData = variables.cropDataCache[variables.crop]
      if (cropData == undefined) {
        loadTable();
      } else {
        loadTableWithLocalData(cropData.columns, cropData.data);
      }
    }
  });
}

function init() {
  variables.cropDataCache = {};
  variables.defaultCropIndex = $("#crop-selection [selected]").index();
}

function loadFilterHTML() {
  $.ajax({
    url: variables.currentURL + "main_filter/" + variables.crop,
    type: "POST",
    dataType: "text",
    success: function(result) {
      $('#cultivar-filter').html('');
      $('#cultivar-filter').html(result);
      customTableFilter();
      initFilter();
      setWidthHalfParent($('.filter-checkbox'));
    }
  });
}

$(function() {
  init();
  loadFilterHTML();
  loadTable();
  cropChange();
});

$(window).on('beforeunload', function() {
  document.getElementById('crop-selection').selectedIndex = variables.defaultCropIndex;
});
