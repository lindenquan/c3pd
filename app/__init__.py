from flask import Flask
from db.DB import DB
import logging

app = Flask(__name__)
app.config.from_object('config.ProductionConfig')
db = DB(app)

import views

def setUpLog():
  logger = logging.getLogger('werkzeug')
  handler = logging.FileHandler(app.config['LOG_FILE'])
  logger.addHandler(handler)
  app.logger.addHandler(handler)

setUpLog()
