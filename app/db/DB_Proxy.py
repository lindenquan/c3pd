from app.models import *
from app import db
import re


class SearchOption:

  def __init__(self, name, options):
    self.name = name
    self.options = options
    self.alias = 'opt-' + ''.join(name.split())

class DB_Proxy:

  def __init__(self, db):
    self.db = db

  def getSearchOptionList(self, cropID):
    options = self.db.session.query(Search).filter_by(crop_type_id=cropID)
    optionList = []
    for option in options:
      optionList.append(option.attribute)
    return optionList

  def getAllCrops(self):
    crops = self.db.session.query(CropType).all()
    cropList = []
    for crop in crops:
      cropList.append(crop.name)
    return cropList

  def getCultivarsByColumns(self, cropID, columns):
    cultivars = self.db.session.query(Cultivar).filter_by(crop_type_id=cropID)
    rows = []
    for cultivar in cultivars:
      # remove all cultivars the name of which contains slash (/)
      if re.match('.*\/.*', cultivar.cultivar_name.strip()):
        continue
      row = []
      for c in columns:
        row.append(getattr(cultivar, c))
      rows.append(row)
    return rows

  def getCultivars(self, crop):
    cropID = self.db.session.query(CropType).filter_by(
        name=crop).first().id_crop_type
    columns = self.getSearchOptionList(cropID)
    rows = self.getCultivarsByColumns(cropID, columns)
    return columns, rows

  def getSearchOptions(self, crop):
    cropID = self.db.session.query(CropType).filter_by(
        name=crop).first().id_crop_type
    options = self.db.session.query(Search).filter_by(crop_type_id=cropID)
    optionList = []
    for option in options:
      if option.attribute == 'cultivar_name':
        continue
      optionList.append(SearchOption(
          option.attribute, option.options.split(Search.delimiter)))
    return optionList

dbProxy = DB_Proxy(db)
