from flask_sqlalchemy import SQLAlchemy

class DB(SQLAlchemy):

  def __init__(self, app):
    SQLAlchemy.__init__(self, app)

  def getOrCreateModel(self, model, **kwargs):
    instance = self.session.query(model).filter_by(**kwargs).first()
    if instance:
      return instance
    else:
      instance = model(**kwargs)
      self.session.add(instance)
      return instance

  def createModel(self, model, **kwargs):
    instance = model(**kwargs)
    self.session.add(instance)
    return instance

  def getAll(self, model):
    return self.session.query(model).all()

