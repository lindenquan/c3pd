from app import db


class CropType(db.Model):
  __tablename__ = 'crop_type'
  id_crop_type = db.Column(db.Integer, primary_key=True, autoincrement=True)
  name = db.Column(db.String(30), nullable=False, unique=True)


class Search(db.Model):
  __tablename__ = 'search'
  delimiter = '@$'
  id_search = db.Column(db.Integer, primary_key=True, autoincrement=True)
  crop_type_id = db.Column(db.Integer, db.ForeignKey('crop_type.id_crop_type'))
  attribute = db.Column(db.String(30), nullable=False)
  options = db.Column(db.Text, nullable=False)


class Card(db.Model):
  __tablename__ = 'card'
  id_card = db.Column(db.Integer, primary_key=True, autoincrement=True, index=True)
  cultivar_id = db.Column(db.Integer, index=True)
  pedigree = db.Column(db.Text)
  pedigree_description = db.Column(db.Text)
  performance = db.Column(db.Text)
  disease_resistance = db.Column(db.Text)
  references = db.Column(db.Text)
  type = db.Column(db.Text)

#
# ! important
# method names should start with underscore _ For instance : _removeDuplicate
# otherwise it will be displayed on cultivar information section.
#


class Cultivar(db.Model):
  __tablename__ = 'cultivar'
  id_cultivar = db.Column(db.Integer, primary_key=True, autoincrement=True, index=True)
  crop_type_id = db.Column(db.Integer, db.ForeignKey('crop_type.id_crop_type'), index=True)
  cultivar_name = db.Column(db.String(200), index=True)
  lowercase_name = db.Column(db.String(200), index=True)
  used_names = db.Column(db.Text)
  parent1 = db.Column(db.Integer, db.ForeignKey('cultivar.id_cultivar'))
  parent2 = db.Column(db.Integer, db.ForeignKey('cultivar.id_cultivar'))
  year_group = db.Column(db.String(45))
  year_registered = db.Column(db.Integer)
  year_released = db.Column(db.Integer)
  year_started = db.Column(db.String(45))
  improvement_status = db.Column(db.String(45))
  breeding_method = db.Column(db.String(45))
  registered = db.Column(db.String(45))
  origin = db.Column(db.String(45))
  cultivar_name_status = db.Column(db.String(45))
  growth_habit = db.Column(db.String(45))
  head_type = db.Column(db.String(45))
  subtype = db.Column(db.String(45))
  affiliation_of_breeders = db.Column(db.Text)
  characteristic = db.Column(db.Text)
  use_type = db.Column(db.String(45))
  growth_habit = db.Column(db.String(45))
  type = db.Column(db.String(45))
  use_class = db.Column(db.String(45))
  released_year_notes = db.Column(db.Text)
  card_id = db.Column(db.Integer, db.ForeignKey('card.id_card'))

  def getCultivarByID(self, id):
    if id != None and id > 0:
      return db.session.query(Cultivar).get(id)
    else:
      return None

  def getParentList(self):
    parentList = [self.getCultivarByID(
        self.parent1), self.getCultivarByID(self.parent2)]
    i = 0
    while i < len(parentList):
      parent = parentList[i]
      if parent != None:
        parentList.append(self.getCultivarByID(parent.parent1))
        parentList.append(self.getCultivarByID(parent.parent2))
      i += 1

    return self._removeDuplicate(parentList)

  def getParentRelationData(self):
    parents = self.getParentList()
    if len(parents) != 0:
      parentNameCoefficientPair = []
      ancestorList = []
      for parent in parents:
        if parent.parent1 == None and parent.parent2 == None:
          ancestorList.append(parent)
        if self.id_cultivar < parent.id_cultivar:
          coefficient = db.session.query(Coefficient).filter_by(cultivar1_id=self.id_cultivar, cultivar2_id=parent.id_cultivar).first()
        else:
          coefficient = db.session.query(Coefficient).filter_by(cultivar1_id=parent.id_cultivar, cultivar2_id=self.id_cultivar).first()
        if(coefficient != None):
          parentNameCoefficientPair.append(
              [parent.cultivar_name, coefficient.coefficient])
      if len(parentNameCoefficientPair) == 0:
        parentNameCoefficientPair = None
      return ancestorList, parents, parentNameCoefficientPair
    else:
      return None, None, None

  def getDirectChildrenList(self):
    childrenList = []
    children = db.session.query(Cultivar).filter_by(parent1=self.id_cultivar)
    if children != None:
      for child in children:
        if child not in childrenList:
          childrenList.append(child)
    children = db.session.query(Cultivar).filter_by(parent2=self.id_cultivar)
    if children != None:
      for child in children:
        if child not in childrenList:
          childrenList.append(child)
    return childrenList

  def _removeDuplicate(self, list):
    cleanList = []
    for cultivar in list:
      if cultivar == None:
        continue
      continueFirstLoop = False
      for c in cleanList:
        if cultivar.id_cultivar == c.id_cultivar:
          continueFirstLoop = True
          break
      if continueFirstLoop:
        continue
      else:
        cleanList.append(cultivar)
    return cleanList

  def getChildList(self):
    childrenList = self.getDirectChildrenList()
    i = 0
    while i < len(childrenList):
      childrenList += childrenList[i].getDirectChildrenList()
      i += 1

    return self._removeDuplicate(childrenList)

  def getChildrenRelationData(self):
    children = self.getChildList()
    if len(children) != 0:
      listRegisteredChildren = []
      childNameCoefficientPair = []
      for child in children:
        if child.registered != None and child.registered.lower() == 'y':
          listRegisteredChildren.append(child)
        if self.id_cultivar < child.id_cultivar:
          coefficient = db.session.query(Coefficient).filter_by(
              cultivar1_id=self.id_cultivar, cultivar2_id=child.id_cultivar).first()
        else:
          coefficient = db.session.query(Coefficient).filter_by(
              cultivar1_id=child.id_cultivar, cultivar2_id=self.id_cultivar).first()
        if(coefficient != None):
          childNameCoefficientPair.append(
              [child.cultivar_name, coefficient.coefficient])
      if (len(childNameCoefficientPair) == 0):
        childNameCoefficientPair = None
      if (len(listRegisteredChildren) == 0):
        listRegisteredChildren = None
      return listRegisteredChildren, children, childNameCoefficientPair
    else:
      return None, None, None

  def explode(self, attr):
    attrObject = getattr(self, attr)
    if attrObject == None:
      return self
    else:
      filterList = ['cultivar_id', 'id_card',
                    'metadata', 'query', 'query_class']
      allValidAttrs = [attr for attr in dir(attrObject) if not attr.startswith(
          '__') and not attr.startswith('_') and attr not in filterList and getattr(attrObject, attr) != None]
      for attr in allValidAttrs:
        setattr(self, attr, getattr(attrObject, attr))


class Coefficient(db.Model):
  __tablename__ = 'coefficient'
  id_coefficient = db.Column(db.Integer, primary_key=True, autoincrement=True, index=True)
  cultivar1_id = db.Column(db.Integer, db.ForeignKey('cultivar.id_cultivar'), index=True)
  cultivar2_id = db.Column(db.Integer, db.ForeignKey('cultivar.id_cultivar'), index=True)
  coefficient = db.Column(db.Float)
