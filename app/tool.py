from flask import render_template, url_for
import os
from os.path import isfile
from app import app
import json

def render(temp, **context):
  temp_name = temp.split('.')[0]
  js = '/static/js/' + temp_name + '.js'
  if isfile(app.config['PROGRAM_HOME'] + js):
    context['js_file'] = url_for('static', filename='js/' + temp_name + '.js')
  css = '/static/style/' + temp_name + '.css'
  if isfile(app.config['PROGRAM_HOME'] + css):
    context['css_file'] = url_for('static', filename='style/' + temp_name + '.css')
  else:
    context['css_file'] = url_for('static', filename='style/default.css')
  return render_template(temp, **context)


def zipdir(path, ziph):
  # ziph is zipfile handle
  for root, dirs, files in os.walk(path):
    for file in files:
      subfile = os.path.join(root, file)
      ziph.write(subfile)

# convert data to json string that could be used by datatable.js
def toDataTableJsonString(columns, rows):
  # sample result
  #'{"data":[["Jack","cook"],["Tom","Engineer"]],"columns":[{ "title": "Name" },{ "title": "Position" }]}'
  columString = '['
  for col in columns:
    columString += '{ "title": "' + col + '" },'
  columString = columString[:-1] + ']'
  return '{"data":' + json.dumps(rows) + ',"columns":' + columString + '}'


