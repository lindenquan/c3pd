from app import app
from tool import render
import view

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    if app.config['ENABLE_CACHE'] == False:
        response.headers['Cache-Control'] = 'public, max-age=0'
    return response

@app.errorhandler(404)
def page_not_found(e):
    return render('404.html'), 404

@app.errorhandler(500)
def page_not_found(e):
    return render('500.html'), 500