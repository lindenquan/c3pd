from app import app, models, db
from app.db.DB_Proxy import dbProxy
from app.tool import render
from os.path import isfile
from flask import url_for
from bs4 import BeautifulSoup
import codecs
from unidecode  import unidecode

APP_HOME = app.config['PROGRAM_HOME']


def getImageURL(cropName, cultivarName):
  cropName = cropName.strip().lower()
  image = APP_HOME + '/static/images/cultivar/' + \
      cropName + '/' + cultivarName + '.jpg'
  if isfile(image):
    return url_for('static', filename='images/cultivar/' + cropName + '/' + cultivarName + '.jpg')
  else:
    return url_for('static', filename='images/cultivar/demo.jpg')


def getValidAttrList(cropID, cultivar):
  if cultivar.card_id != None:
    with db.session.no_autoflush:
      cultivar.card = db.session.query(models.Card).get(cultivar.card_id)
      cultivar.explode('card')
  filterList = app.config['FILTER_LIST']
  allValidAttrs = [attr for attr in dir(cultivar) if not attr.startswith('get') if not attr.startswith('__') and not attr.startswith(
      '_') and attr not in filterList and getattr(cultivar, attr) != None]
  return allValidAttrs


#  Example Tree='''
#<ul class="tree">
#  <li><a href="#about">About</a>
#    <ul>
#      <li><a href="#">Contact</a></li>
#      <li><a href="#">Blog</a></li>
#      <li><a href="#">Jobs</a>
#        <ul>
#          <li><a href="#jobs1">Job 1</a></li>
#          <li><a href="#jobs2">Job 2</a></li>
#          <li><a href="#jobs3">Job 3</a></li>
#        </ul>
#      </li>
#    </ul>
#  </li>
#</ul>
#'''

def subTree(cultivar):
  begin = '<li><a class="cultivar-link" href="javascript:void(0)">' + cultivar.cultivar_name + '</a>'
  end = '</li>'
  if cultivar.parent1 == None and cultivar.parent2 == None:
    return begin + end

  result = begin + '<ul>'
  if cultivar.parent1 != None:
    result += subTree(db.session.query(models.Cultivar).get(cultivar.parent1))
  if cultivar.parent2 != cultivar.parent1 and cultivar.parent2 != None:
    result += subTree(db.session.query(models.Cultivar).get(cultivar.parent2))
  result += '</ul>' + end
  return result

def generateTreeHTML(cultivar):
  return '<ul class="tree">' + subTree(cultivar) +'</ul>'

def getSVG(cropName, cultivarName):
  cropName = cropName.strip().lower()
  image = APP_HOME + '/static/images/cultivar/' + \
      cropName + '/pedigree/' + cultivarName + '.svg'

  if isfile(image):
    soup = BeautifulSoup(codecs.open(image, 'r', 'utf8'), 'html.parser')
    soup = soup.svg
    #for tspan in soup.find_all('tspan'):
    #  if tspan.get_text() == '-':
    #    hyphenG = tspan.parent.parent.parent
    #    previousG = hyphenG.previous_sibling
    #    nextG = hyphenG.next_sibling
    #    newString = ''
    #    firstTspan = None
    #    for preTspan in previousG.find_all('tspan'):
    #      firstTspan = preTspan
    #      newString = preTspan.get_text()
    #    for nextTspan in nextG.find_all('tspan'):
    #      newString +='-'
    #      newString += nextTspan.get_text()
    #    if firstTspan != None:
    #      firstTspan.string =newString
    #    hyphenG.extract()
    #    nextG.extract()
    return unidecode(soup)
  else:
    return None

@app.route('/cultivar/<cropName>/<path:cultivarName>', methods=['GET'])
def cultivar(cropName, cultivarName):
  cultivarName = cultivarName.replace('<s>', '/')
  title = cropName.capitalize() + ': ' + cultivarName
  global APP_HOME
  cropName = cropName.strip().lower()
  crop = db.session.query(models.CropType).filter_by(name=cropName).first()
  cultivar = db.session.query(models.Cultivar).filter_by(
      crop_type_id=crop.id_crop_type, lowercase_name=cultivarName.lower()).first()
  if cultivar == None:
    return render('cultivarNotFound.html',cultivar=cultivarName, crop=cropName.capitalize())
  listAncestors, listParents, coefficientParents = cultivar.getParentRelationData()
  listRegisteredChildren, listChildren, coefficientChildren = cultivar.getChildrenRelationData()

  treeHTML = generateTreeHTML(cultivar)
  svg = getSVG(cropName, cultivarName)
  cultivar.image = getImageURL(cropName, cultivarName)
  # parent1 and parent2 become string real name instead of id
  # so the following code should be at last
  if cultivar.parent1 != None and cultivar.parent1 > 0:
    cultivar.parent1 = db.session.query(
        models.Cultivar).get(cultivar.parent1).cultivar_name
  if cultivar.parent2 != None and cultivar.parent2 > 0:
    cultivar.parent2 = db.session.query(
        models.Cultivar).get(cultivar.parent2).cultivar_name
  attrs = getValidAttrList(crop.id_crop_type, cultivar)

  return render('cultivar.html', svg=svg, treeHTML=treeHTML, title=title, listRegisteredChildren=listRegisteredChildren, listAncestors=listAncestors, cropName=cropName, cultivar=cultivar, attrs=attrs, listParents=listParents, listChildren=listChildren, coefficientParents=coefficientParents, coefficientChildren=coefficientChildren)
