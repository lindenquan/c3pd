from app import app
from flask import request
from app.tool import render


@app.route('/about', methods=['GET'])
def about():
  return render('about.html', title='About')