from app import app
from app.db.DB_Proxy import dbProxy
from flask import request
from app.tool import render, toDataTableJsonString
from app import models


@app.route('/', methods=['GET'])
def index():
  crops = dbProxy.getAllCrops()
  crop = request.args.get('crop')
  keyword = request.args.get('keyword')
  if crop is None:
    crop = app.config['DEFAULT_CROP']

  return render('index.html', crops=crops, crop=crop, keyword=keyword, title=crop.capitalize() + ': cultivar list')


@app.route('/main_table/<crop>', methods=['POST'])
def main_table(crop):
  # sample result
  #'{"data":[["Jack","cook"],["Tom","Engineer"]],"columns":[{ "title": "Name" },{ "title": "Position" }]}'
  columns, rows = dbProxy.getCultivars(crop)
  return toDataTableJsonString(columns, rows)


@app.route('/main_filter/<crop>', methods=['POST'])
def main_filter(crop):
  searchOptions = dbProxy.getSearchOptions(crop)
  return render('main_filter.html', searchOptions=searchOptions)
