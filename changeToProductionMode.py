
def changeToProductionMode():
  fileName = 'app/__init__.py'
  content = ''
  with open(fileName, 'r') as f:
    for line in f:
      if line.strip() == "app.config.from_object('config.DevelopmentConfig')":
        line = "app.config.from_object('config.ProductionConfig')"
      content += line
      
  with open(fileName, 'w') as f:
    f.write(content)

changeToProductionMode()