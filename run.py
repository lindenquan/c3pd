#!/usr/bin/python
# -*- coding: utf-8 -*-

from app import app as application

if __name__ == "__main__":
  application.run(host=application.config['HOST'], port=application.config['PORT'])
